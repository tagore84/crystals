package es.pozoesteban.alberto.crystal;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.games.GameType;
import es.pozoesteban.alberto.crystal.model.move_codes.ReinforcementLearningMove;
import es.pozoesteban.alberto.crystal.model.players.*;

import java.util.Date;

import static es.pozoesteban.alberto.crystal.games.GameType.DYNAMIC_TRAINING;

public abstract class Configuration {

    public static abstract class GeneralConfiguration {
        public static final GameType GAME_TYPE = DYNAMIC_TRAINING;
        public static final int LOG_LEVEL = Logger.INFO; // ToDo No fuciona, siempre saca INFO
        public static final Class MOVE_CLASS = ReinforcementLearningMove.class;
        public static final long GAME_SEED = new Date().getTime();
        public static final String LOCAL = "../../net_training/ds/";
        public static final String DATASET_FOLDER = LOCAL + "rl_vs_v01/";
        public static final boolean SAVE_MOVES = false;
    }
    public static abstract class RealGameConfiguration {
        public static final int NUM_PLAYERS = 2;
        public static final boolean SCREEN_DEBUG = false;
        public static final Class[] REAL_GAME_PLAYER_CLASSES = new Class[]{
                HumanPlayer.class,
                DynamicPunset.class
        };
    }
    public static abstract class SaveMovesGameConfiguration {
        public static final int NUM_SETS = 1;
        public static final int GAMES_PER_SET = 10;    // 20 MB -> X de 20 pts, 4.000 de 25 pts, 10000 de 30 pts
        public static final int MIN_POINTS_TO_SAVE_MOVES = 30;
        public static final Class TRAINING_PLAYER_ONE_CLASS = JuanitoElAleatorio.class;
        public static final Class TRAINING_PLAYER_TWO_CLASS = ReinforcementLearningStaticPunset.class;
    }
    public static abstract class DynamicTrainingGameConfiguration {
        public static final int NUM_SETS = 45;
        public static final int GAMES_PER_SET = 30;
        public static final int MODEL_BASE_SEED = 1;
        public static final Player[] PLAYERS = new Player[] {
            new DynamicPunset(0, "A", new int[] {50}, new int[] {5}),
            new DynamicPunset(1, "B", new int[] {55, 40}, new int[] {15, 10}),
            new DynamicPunset(2, "C", new int[] {60, 40, 20}, new int[] {10, 15, 10}),
            new DynamicPunset(3, "D", new int[] {70, 50, 30, 20}, new int[] {10, 20, 15, 10}),
        };
    }
}
