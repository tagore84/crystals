package es.pozoesteban.alberto.crystal;

import com.badlogic.gdx.Game;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.move_codes.Move;
import es.pozoesteban.alberto.crystal.model.players.Player;
import es.pozoesteban.alberto.crystal.screens.BoardScreen;

import java.util.HashMap;
import java.util.List;

public abstract class CrystalsGame extends Game {

	//<editor-fold desc="Other propperties">
	//</editor-fold>

	protected GameState gameState;
	protected List<Move> movesLogs;
	protected HashMap<Player, Integer> results;
	protected int numFilesInFolder;

	public GameState getGameState() {
		return gameState;
	}

	public abstract void create();
	public abstract void onCurrentPlayerEndTurn();
	public abstract BoardScreen getBoardScreen();
}
