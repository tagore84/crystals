package es.pozoesteban.alberto.crystal;

//import com.badlogic.gdx.utils.Logger;
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.IOException;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class TestKerasImport {

    //private static Logger LOG = new Logger("TestKerasImport", LOG_LEVEL);
    private static Logger LOG = LoggerFactory.getLogger(TestKerasImport.class); // Keras style


    public static void main(String[] args) throws IOException, InvalidKerasConfigurationException, UnsupportedKerasConfigurationException {
        //LOG

        LOG.info("This is how you configure Java Logging with SLF4J");

        // load the model
        //ClassPathResource path = new ClassPathResource("..\\..\\net_training\\select_crystals_model_v0.1.h5");
        File modelFile = new File("C:\\Users\\Albertolau\\src\\crystals\\net_training\\select_crystals_model_v0.1.h5");
        String simpleMlp = modelFile.getPath();
        MultiLayerNetwork model = KerasModelImport.importKerasSequentialModelAndWeights(simpleMlp);

        // make a random sample
        INDArray features = Nd4j.zeros(1, 35);
        for (int i=0; i<35; i++) {
            features.putScalar(new int[]{i}, Math.random() < 0.5 ? 0 : 1);
        }

        // get the prediction
        INDArray prediction = model.output(features);

        LOG.info("Prediction " + prediction.toString());
    }
}
