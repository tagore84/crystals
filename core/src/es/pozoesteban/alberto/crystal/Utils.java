package es.pozoesteban.alberto.crystal;


import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.model.move_codes.Move;

import java.io.*;
import java.util.List;
import java.util.Random;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class Utils {

    private static Random DICE = new Random(GAME_SEED);
    private static Logger LOG = new Logger("Utils", LOG_LEVEL);

    public static float nextFloat() {
        return DICE.nextFloat();
    }

    public static int[] randomInWeightMatrix(float[][] weights, float sum) {
        float originalValue = DICE.nextFloat();
        float debugSum = 0f;
        float value = originalValue * sum;
        for (int i = 0; i < weights.length; i++) {
            for (int j = 0; j < weights[i].length; j++) {
                debugSum += weights[i][j];
                if (weights[i][j] >= value) return new int[]{i, j};
                value -= weights[i][j];
            }
        }
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }
    public static int getBestOption(float[] weights, float sum) {
        float max = 0;
        int bestOption = -1;
        for (int i = 0; i < weights.length; i++) {
            if (weights[i] >= max) {
                max = weights[i];
                bestOption = i;
            }
        }
        if (bestOption >= 0) return bestOption;
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }
    public static int[] getBestOption(float[][] likelihood) {
        float max = 0;
        int[] bestOption = new int[]{-1, -1};
        for (int i = 0; i < likelihood.length; i++) {
            for (int j = 0; j < likelihood[i].length; j++) {
                if (likelihood[i][j] >= max) {
                    max = likelihood[i][j];
                    bestOption[0] = i;
                    bestOption[1] = j;
                }
            }
        }
        if (bestOption[0] >= 0) return bestOption;
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }
    public static int randomInWeightArray(float[] weights, float sum) {
        float value = DICE.nextFloat() * sum;
        for (int i = 0; i < weights.length; i++) {
            if (weights[i] >= value) return i;
            value -= weights[i];
        }
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }
    public static int[] randomInWeightArray(float[][] likelihood, float sum) {
        float value = DICE.nextFloat() * sum;
        for (int i = 0; i < likelihood.length; i++) {
            for (int j = 0; j < likelihood[i].length; j++) {
                {
                    if (likelihood[i][j] >= value) return new int[]{i, j};
                    value -= likelihood[i][j];
                }
            }
        }
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }

    public static int nextInt(int max) {
        return DICE.nextInt(max);
    }

    private static String selectCrystalsMovesLogToString(List<Move> movesLogs, boolean addInitialBracket) {
        StringBuilder sb = new StringBuilder();
        if (addInitialBracket) sb.append("[");
        for (Move move : movesLogs) {
            sb.append(move.toSelectCrystalsExample());
            sb.append(",").append(System.lineSeparator());
        }
        sb.delete(sb.lastIndexOf(","), sb.length());
        sb.append("]");
        return sb.toString();
    }
    private static String selectRowMovesLogToString(List<Move> movesLogs, boolean addInitialBracket) {
        StringBuilder sb = new StringBuilder();
        if (addInitialBracket) sb.append("[");
        for (Move move : movesLogs) {
            sb.append(move.toSelectRowExample());
            sb.append(",").append(System.lineSeparator());
        }
        sb.delete(sb.lastIndexOf(","), sb.length());
        sb.append("]");
        return sb.toString();
    }

    public static void saveMovesInFolder(String selectCrystalsDatasetFolder, List<Move> movesLogs, int index) throws IOException {
        File directory1 = new File(selectCrystalsDatasetFolder + "/crystals");
        if (!directory1.exists()){
            directory1.mkdirs();
        }
        String text1 = selectCrystalsMovesLogToString(movesLogs, true);
        File file = new File(directory1 + "/ds_s_" + String.format("%03d", index) + ".json");
        FileOutputStream fos = new FileOutputStream(file, true);
        fos.write(text1.getBytes());
        fos.close();
        LOG.info(file.toString() + " saved!");

        File directory2 = new File(selectCrystalsDatasetFolder + "/rows");
        if (!directory2.exists()){
            directory2.mkdirs();
        }
        String text2 = selectRowMovesLogToString(movesLogs, true);
        File file2 = new File(directory2 + "/ds_s_" + String.format("%03d", index)  + ".json");
        FileOutputStream fos2 = new FileOutputStream(file2, true);
        fos2.write(text2.getBytes());
        fos2.close();
        LOG.info(file2.toString() + " saved!");
    }

    public static int countFilesInFolder(String datasetFolder) {
        File directory = new File(datasetFolder + "/crystals");
        if (!directory.exists()){
            return 0;
        }
        return new File(directory.getPath()).list().length;
    }

    public static int getNumFactories(int numPlayers) {
        switch (numPlayers) {
            case 2:
                return 5;
            case 3:
                return 7;
            default:
                return 9;
        }
    }

    public static float[][] arrayToMatrix(float[] likelihoodArray, int rows, int columns) {
        float[][] matrix = new float[rows][columns];
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < columns; c++) {
                matrix[r][c] = likelihoodArray[r*columns + c];
            }
        }
        return matrix;
    }
}
