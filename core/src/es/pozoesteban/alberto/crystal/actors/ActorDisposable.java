package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class ActorDisposable extends Actor {
    public abstract void dispose();
}
