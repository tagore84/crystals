package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.MARGIN;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class CrystalActor extends ActorDisposable {

    private final static Logger LOG = new Logger("Crystal", LOG_LEVEL);

    private final CrystalsGame game;

    private final Crystal crystal;

    private static final float WITH = 64f;
    private static final float HEIGHT = 64f;

    protected Texture crystalTexture;
    protected Texture crystalSelectedTexture;
    private boolean isVisible;

    public CrystalActor(final CrystalsGame game, final Crystal crystal) {
        setWidth(WITH * SIZE_FACTOR);
        setHeight(HEIGHT * SIZE_FACTOR);

        setX((crystal.getId()%15) * getWidth());
        setY((crystal.getId()/15) * getHeight());

        this.crystal = crystal;
        this.game = game;
        switch (crystal.getColor()) {
            case BLUE:
                crystalTexture = new Texture("crystal_blue.png");
                crystalSelectedTexture = new Texture("crystal_blue_selected.png");
                break;
            case RED:
                crystalTexture = new Texture("crystal_red.png");
                crystalSelectedTexture = new Texture("crystal_red_selected.png");
                break;
            case YELLOW:
                crystalTexture = new Texture("crystal_yellow.png");
                crystalSelectedTexture = new Texture("crystal_yellow_selected.png");
                break;
            case WHITE:
                crystalTexture = new Texture("crystal_white.png");
                crystalSelectedTexture = new Texture("crystal_white_selected.png");
                break;
            case ORANGE:
                crystalTexture = new Texture("crystal_orange.png");
                crystalSelectedTexture = new Texture("crystal_orange_selected.png");
                break;
            case FIRST_TOKEN:
                crystalTexture = new Texture("first_token.png");
        }
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                LOG.info(crystal.toLongString());
                if (crystal.isInTheFactories() && crystal.getColor() != Color.FIRST_TOKEN) {
                    game.getGameState().takeCrystalsFromFactory(crystal);
                }
            }
        });

    }
    @Override
    public void dispose() {
        if (crystalTexture != null) crystalTexture.dispose();
        if (crystalSelectedTexture != null) crystalSelectedTexture.dispose();
    }

    @Override
    public void act (float delta) {
        if (crystal.hasChanged()) {
            float[] pos = calcPosition();
            setPosition(pos[0], pos[1]);
            isVisible = !(crystal.isInTheSack() || crystal.isInTheGarbage());
            crystal.hasChanged(false);
        }
    }

    private float[] calcPosition() {
        if (crystal.isInTheSack()) return new float[]{-1 * getWidth(), -1 * getHeight()};
        if (crystal.isInTheGarbage()) return new float[]{-1 * getWidth(), -1 * getHeight()};
        if (crystal.isInTheFactories()) {
            int factory = crystal.getInFactory();
            int index = crystal.getInFactoryIndex();
            return calcFactoryPosition(factory, index);
        }
        if (crystal.isInTheBoardPlayers()) {
            int player = crystal.getInBoardPlayer();
            int row = crystal.getInBoardPlayerRow();
            int column  = crystal.getInBoardPlayerColumn();
            return calcPlayerBoardPosition(player, row, column);
        }
        throw new IllegalArgumentException("Estado absurdo de " + crystal);
    }
    private float[] calcPlayerBoardPosition(int playerNumber, int row, int column) {
        PlayerBoardActor playerBoard = game.getBoardScreen().getPlayerBoardActors(playerNumber);
        float[] offset = calcOffset(row, column);
        switch (playerNumber) {
            case 0:
                return new float[]{playerBoard.getX() + offset[0], playerBoard.getY() + offset[1]};
            case 1:
                return new float[]{playerBoard.getX() + playerBoard.getWidth() - offset[0] - getWidth(),
                        playerBoard.getY() + playerBoard.getHeight() - offset[1] - getHeight()};
            case 2:
                return new float[]{playerBoard.getX() + offset[1],
                        playerBoard.getY() + playerBoard.getHeight() - offset[0] - getHeight()};
            case 3:
                return new float[]{playerBoard.getX() + playerBoard.getWidth() - offset[1] - getWidth(),
                        playerBoard.getY() + offset[0]};
        }
        throw new IllegalArgumentException("Estado absurdo de " + crystal);
    }

    private float[] calcOffset(int row, int column) {
        float x = 48f * SIZE_FACTOR;
        x += row == 0 ? column * (90f * SIZE_FACTOR) : column * (85f * SIZE_FACTOR);
        if (column > 4 && row > 0) x += (25 * SIZE_FACTOR);
        float y = row == 0 ? 145f * SIZE_FACTOR : 203f * SIZE_FACTOR;
        y += (row-1) * (86f * SIZE_FACTOR);
        return new float[]{x, y};
    }

    private float[] calcFactoryPosition(int factoryNumber, int index) {
        if (factoryNumber < 10) {
            FactoryActor factory = game.getBoardScreen().getFactoryActors(factoryNumber);
            float x, y;
            if (index == 0 || index == 2) {
                x = factory.getX() + (factory.getWidth() / 5f);
            } else {
                x = factory.getX() + factory.getWidth() - (factory.getWidth() / 5f) - getWidth();
            }
            if (index == 0 || index == 3) {
                y = factory.getY() + (factory.getHeight() / 5f);
            } else {
                y = factory.getY() + factory.getHeight() - (factory.getHeight() / 5f) - getHeight();
            }
            return new float[]{x, y};
        } else { // Center Plate...
            PlateActor factory = game.getBoardScreen().getCenterPlate();
            float x0 = factory.getX() + (MARGIN/2f);
            float x1 = factory.getX() + factory.getWidth() - (MARGIN/2f);
            float y0 = factory.getY() + (MARGIN/2f);
            float y1 = factory.getY() + factory.getHeight() - (MARGIN/2f);
            return game.getBoardScreen().calcRandomPositionInTheCenterPlate();
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (isVisible) { // ToDo eliminar el true
            if (crystal.isSelected()) {
                batch.draw(crystalSelectedTexture, getX(), getY(), getWidth(), getHeight());
            } else {
                batch.draw(crystalTexture, getX(), getY(), getWidth(), getHeight());
            }
        }
    }

    public Crystal getCrystal() {
        return crystal;
    }
}
