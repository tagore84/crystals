package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Logger;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.*;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class FactoryActor extends ActorDisposable {

    private final static Logger LOG = new Logger("FactoryActor", LOG_LEVEL);

    private static final float WITH = 256f;
    private static final float HEIGHT = 256f;
    private static final float X_MARGIN = 5f;
    private static final float Y_MARGIN = 5f;

    protected Texture factoryTexture;
    private final int index;

    public FactoryActor(int index, int players) {
        this.index = index;
        factoryTexture = new Texture("factory_" + index + ".png");
        setWidth(WITH * SIZE_FACTOR);
        setHeight(HEIGHT * SIZE_FACTOR);
        float[] pos = calcPosition(index);
        setPosition(pos[0], pos[1]);
    }

    private float[] calcPosition(int index) {
        // 4 3 2 1 0 1 2 3 4  x/y
        // 7   3   0   4   8  0
        //   5   1   2   6    1
        int indexX = -1;
        int indexY = -1;
        switch (index) {
            case 0:
                indexX = 0;
                indexY = 0;
                break;
            case 1:
                indexX = -1;
                indexY = 1;
                break;
            case 2:
                indexX = 1;
                indexY = 1;
                break;
            case 3:
                indexX = -2;
                indexY = 0;
                break;
            case 4:
                indexX = 2;
                indexY = 0;
                break;
            case 5:
                indexX = -3;
                indexY = 1;
                break;
            case 6:
                indexX = 3;
                indexY = 1;
                break;
            case 7:
                indexX = -4;
                indexY = 0;
                break;
            case 8:
                indexX = 4;
                indexY = 0;
                break;
        }
        float x = (WIDTH_SCREEN / 2f) - getWidth()/2f;
        x += indexX * ((getWidth()/2f) + X_MARGIN);
        float y = indexY == 1 ? (HEIGHT_SCREEN / 2) - getHeight() - (Y_MARGIN / 2f) : (HEIGHT_SCREEN / 2) + (Y_MARGIN / 2f);
        return new float[]{x, y};
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(factoryTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        factoryTexture.dispose();
    }
}
