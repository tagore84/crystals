package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;


import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.*;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class PlateActor extends ActorDisposable {

    private final static Logger LOG = new Logger("PlateActor", LOG_LEVEL);

    private static final float WITH = 512f;
    private static final float HEIGHT = 512f;

    protected Texture sackTexture;

    public PlateActor() {
        sackTexture = new Texture("plate.png");
        setWidth(WITH * SIZE_FACTOR);
        setHeight(HEIGHT * SIZE_FACTOR);
        setPosition(MARGIN + getWidth(), HEIGHT_SCREEN - getHeight() - MARGIN);
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                LOG.info("[" + getX() + ", " + (getX() + getWidth()) + ", " + getY() + ", " + (getY()) + getHeight() + "]");
            }
        });
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(sackTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        sackTexture.dispose();
    }
}
