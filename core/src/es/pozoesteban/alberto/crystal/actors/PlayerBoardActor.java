package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.Player;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.*;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class PlayerBoardActor extends ActorDisposable {

    private final static Logger LOG = new Logger("CardActor", LOG_LEVEL);

    private static final float WITH = 950f;
    private static final float HEIGHT = 686f;
    protected Texture playerBoardTexture;
    protected TextureRegion playerBoardRegion;
    protected Texture playerBoardTextureCurrent;
    protected TextureRegion playerBoardRegionCurrent;

    private final int index;
    private final GameState gameState;

    public PlayerBoardActor(int index, final GameState gameState) {
        this.index = index;
        this.gameState = gameState;
        playerBoardTexture = new Texture("player_board_950_686_" + index + ".png");
        playerBoardTextureCurrent = new Texture("player_board_950_686_" + index + "_current.png");
        switch (index) {
            case 0:
                playerBoardRegion = new TextureRegion(playerBoardTexture, 0, 0, 950, 686);
                playerBoardRegionCurrent = new TextureRegion(playerBoardTextureCurrent, 0, 0, 950, 686);
                setWidth(WITH * SIZE_FACTOR);
                setHeight(HEIGHT * SIZE_FACTOR);
                setX((WIDTH_SCREEN/2) - (getWidth()/2));
                setY(MARGIN);
                break;
            case 1:
                playerBoardRegion = new TextureRegion(playerBoardTexture, 0, 0, 950, 686);
                playerBoardRegionCurrent = new TextureRegion(playerBoardTextureCurrent, 0, 0, 950, 686);
                setWidth(WITH * SIZE_FACTOR);
                setHeight(HEIGHT * SIZE_FACTOR);
                setX((WIDTH_SCREEN/2) - (getWidth()/2) + 40);
                setY(HEIGHT_SCREEN - (getHeight() + MARGIN));
                break;
            case 2:
                playerBoardRegion = new TextureRegion(playerBoardTexture, 0, 0, 686, 950);
                playerBoardRegionCurrent = new TextureRegion(playerBoardTextureCurrent, 0, 0, 686, 950);
                setWidth(HEIGHT * SIZE_FACTOR);
                setHeight(WITH * SIZE_FACTOR);
                setX(MARGIN);
                setY((HEIGHT_SCREEN/ 2) - (getHeight()/2));
                break;
            case 3:
                playerBoardRegion = new TextureRegion(playerBoardTexture, 0, 0, 686, 950);
                playerBoardRegionCurrent = new TextureRegion(playerBoardTextureCurrent, 0, 0, 686, 950);
                setWidth(HEIGHT * SIZE_FACTOR);
                setHeight(WITH * SIZE_FACTOR);
                setX(WIDTH_SCREEN - (getWidth() + MARGIN));
                setY((HEIGHT_SCREEN/ 2) - (getHeight()/2) - 40);
                break;
        }
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                for (Player player : gameState.getPlayers()) {
                    if(player.hasFinished()) Gdx.app.exit();
                }
            }
        });
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (gameState.getCurrentPlayer().getId() == index) {
            batch.draw(playerBoardRegionCurrent, getX(), getY(), getWidth(), getHeight());
        } else {
            batch.draw(playerBoardRegion, getX(), getY(), getWidth(), getHeight());
        }
    }

    public void dispose() {
        playerBoardTextureCurrent.dispose();
        playerBoardTexture.dispose();
    }
}
