package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.Player;

import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class PointsActor extends ActorDisposable {

    protected final BitmapFont points;
    protected final GameState gameState;

    public PointsActor(GameState gameState) {
        this.points = new BitmapFont();
        this.points.setColor(0,0,0, 1);
        this.gameState = gameState;
        setX(SIZE_FACTOR * 100);
        setY(SIZE_FACTOR * 500);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        points.draw(batch, getText(), getX(), getY());
    }

    private String getText() {
        String output = "";
        for (Player player : gameState.getPlayers()) {
            output += player.getName() + " -> " + player.getPoints() + System.lineSeparator();
        }
        return output;
    }

    @Override
    public void dispose() {
        points.dispose();
    }
}
