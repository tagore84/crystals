package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.MARGIN;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class RowSelectorActor extends ActorDisposable {

    private final static Logger LOG = new Logger("RowSelectorActor", LOG_LEVEL);

    private final CrystalsGame game;

    private final int row;

    private static final float WITH = 128f;
    private static final float HEIGHT = 64f;

    protected Texture rowSelectorTexture;

    public RowSelectorActor(final CrystalsGame game, final int row) {
        this.row = row;
        this.game = game;
        rowSelectorTexture = new Texture("row_selector.png");
        setWidth(WITH * SIZE_FACTOR);
        setHeight(HEIGHT * SIZE_FACTOR);

        setX(game.getBoardScreen().getPlayerBoardActors(0).getX() - getWidth() - (MARGIN/2f));
        setY((285f * SIZE_FACTOR) + ((row-1) * (85f * SIZE_FACTOR)));
        if (row == 0) setY(getY() - 70F * SIZE_FACTOR);
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                game.getGameState().selectRow(row);
                game.onCurrentPlayerEndTurn();
            }
        });
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        if (game.getGameState().selectingRow(row)) {
            batch.draw(rowSelectorTexture, getX(), getY(), getWidth(), getHeight());
        }
    }

    public void dispose() {
        rowSelectorTexture.dispose();
    }
}
