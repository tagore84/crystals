package es.pozoesteban.alberto.crystal.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.screens.BaseScreen.*;
import static es.pozoesteban.alberto.crystal.screens.BoardScreen.SIZE_FACTOR;

public class SackActor extends ActorDisposable {

    private final static Logger LOG = new Logger("SackActor", LOG_LEVEL);

    private final CrystalsGame game;


    private static final float WITH = 512f;
    private static final float HEIGHT = 512f;

    protected Texture sackTexture;

    public SackActor(final CrystalsGame game) {
        this.game = game;
        sackTexture = new Texture("sack.png");
        setWidth(WITH * SIZE_FACTOR);
        setHeight(HEIGHT * SIZE_FACTOR);
        setPosition(MARGIN, HEIGHT_SCREEN - getHeight() - MARGIN);
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(sackTexture, getX(), getY(), getWidth(), getHeight());
    }

    public void dispose() {
        sackTexture.dispose();
    }
}
