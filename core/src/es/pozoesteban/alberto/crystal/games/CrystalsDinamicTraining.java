package es.pozoesteban.alberto.crystal.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.DynamicPunset;
import es.pozoesteban.alberto.crystal.model.players.Player;
import es.pozoesteban.alberto.crystal.screens.BoardScreen;
import freemarker.template.SimpleDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.Configuration.DynamicTrainingGameConfiguration.*;

public class CrystalsDinamicTraining extends CrystalsGame {

    private long start;
    private int numEpoch;
    private int epochDone;
    protected final int numPlayers = 2;
    private static Logger LOG = new Logger("CrystalsDinamicTraining", LOG_LEVEL);

    private List<Player> currentPlayers;
    private List<List<Player>> allVs;

    private static SimpleDateFormat SDF = new SimpleDateFormat("EEEE HH:mm:ss");



    @Override
    public void create() {
        start = new Date().getTime();
        epochDone = 0;
        results = new HashMap();
        allVs = new ArrayList();
        for (int i = 0;  i < PLAYERS.length-1; i++) {
            for (int j = i + 1; j < PLAYERS.length; j++) {
                List<Player> currentVs = new ArrayList();
                currentVs.add(PLAYERS[i]);
                currentVs.add(PLAYERS[j]);
                allVs.add(currentVs);
            }
        }
        numEpoch = allVs.size() * NUM_SETS;
        try {
            for (int i = 0; i < NUM_SETS; i++){
                playOneSetBattleGames(i);
            }
            Gdx.app.exit();
        } catch (Exception ex) {
            LOG.error("Fatal error, closing", ex);
        }
    }

    private void playOneSetBattleGames(int index) {
        for (List<Player> current : allVs) {
            String estimatedFinishTime = "-";
            if (epochDone > 0) {
                long now = new Date().getTime();
                long epochDuration = (now - start) / epochDone;
                long estimated = now + (epochDuration * (numEpoch - epochDone));
                estimatedFinishTime = SDF.format(new Date(estimated));
            }
            currentPlayers = current;
            LOG.info("Starting epoch " + index + " "
                    + currentPlayers.get(0).getName() + " vs " + currentPlayers.get(1).getName()
                    + " estimated finish time: " + estimatedFinishTime);
            for (int i = 0; i < GAMES_PER_SET; i++) {
                gameState = new GameState(currentPlayers, Utils.getNumFactories(numPlayers), Utils.nextFloat() > 0.5f ? 0 : 1);
                startGame();
            }
            epochDone++;
            LOG.info("\t" + currentPlayers.get(0).showGamePoints());
            currentPlayers.get(0).resetGamePoints();
            LOG.info("\t" + currentPlayers.get(1).showGamePoints() + System.lineSeparator());
            currentPlayers.get(1).resetGamePoints();
        }
        LOG.info("Training...");
        for (Player player : PLAYERS) {
            if (player instanceof DynamicPunset) {
                ((DynamicPunset) player).trainEpoch();
                ((DynamicPunset) player).saveModel(index);
            }
        }
        LOG.info("Done" + System.lineSeparator());
    }

    //<editor-fold desc="Match management">
    private void startGame() {
        gameState.takeCrystalsFromSack();
        onStartTurn();
    }
    private void endGame() {
        Player player0 = currentPlayers.get(0);
        Player player1 = currentPlayers.get(1);

        if (player0.getPoints() < player1.getPoints() | player0.getPoints() == 0) {
            ((DynamicPunset)player0).youWin(false);
        } else {
            ((DynamicPunset)player0).youWin(true);
        }
        if (player1.getPoints() < player0.getPoints() | player1.getPoints() == 0) {
            ((DynamicPunset)player1).youWin(false);
        } else {
            ((DynamicPunset)player1).youWin(true);
        }
        player0.saveGamePoints(player0.getPoints(), player1.getPoints());
        player1.saveGamePoints(player1.getPoints(), player0.getPoints());
        for (Player player : currentPlayers) {
            player.reset();
        }
    }
    //</editor-fold>

    //<editor-fold desc="Turn management">
    private void onStartTurn() {
        Player currentPlayer = gameState.getCurrentPlayer();
        currentPlayer.onBegingTurn(this);
    }
    @Override
    public void onCurrentPlayerEndTurn() {
        if (gameState.noMoreCrystalsInFactories()) {
            for (Player player : gameState.getPlayers()) {
                player.pointCount();
                if (player.hasFinished()) {
                    endGame();
                    return;
                }
            }
            gameState.nextPlayer(true);
            gameState.takeCrystalsFromSack();
        } else {
            gameState.nextPlayer(false);
        }
        onStartTurn();
    }
    //</editor-fold>

    @Override
    public BoardScreen getBoardScreen() {
        return null;
    }
}
