package es.pozoesteban.alberto.crystal.games;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.HumanPlayer;
import es.pozoesteban.alberto.crystal.model.players.Player;
import es.pozoesteban.alberto.crystal.model.players.ReinforcementLearningStaticPunset;
import es.pozoesteban.alberto.crystal.screens.BoardScreen;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.Configuration.RealGameConfiguration.*;

public class CrystalsRealGame extends CrystalsGame {



    //<editor-fold desc="Other propperties">
    private static Logger LOG = new Logger("CrystalsRealGame", LOG_LEVEL);
    private BoardScreen boardScreen;
    //</editor-fold>


    @Override
    public void create() {
        boardScreen = new BoardScreen(this);
        movesLogs = new ArrayList();
        results = new HashMap();
        numFilesInFolder = Utils.countFilesInFolder(DATASET_FOLDER);
        try {
            configuratedGame();
        } catch (Exception ex) {
            LOG.error("Error en el juego manual configurado", ex);
        }
    }
    private void configuratedGame() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Player> players = new ArrayList();
        for (int i = 0; i < REAL_GAME_PLAYER_CLASSES.length; i++) {
            Constructor<?> constructor = REAL_GAME_PLAYER_CLASSES[i].getConstructor(Integer.class);
            players.add((Player) constructor.newInstance(new Object[] {i}));
        }
        gameState = new GameState(players, Utils.getNumFactories(NUM_PLAYERS), 1);
        boardScreen.init();
        setScreen(boardScreen);
        startGame(0);
    }

    private void debugManualGameHumanVsPunset() {
        List<Player> players = new ArrayList();
        players.add(new HumanPlayer(0, "Alberto"));
        players.add(new ReinforcementLearningStaticPunset(1));

        gameState = new GameState(players, Utils.getNumFactories(NUM_PLAYERS), 1);
        boardScreen.init();
        setScreen(boardScreen);
        startGame(0);
    }

    //<editor-fold desc="Match management">
    private void startGame(int gameId) {
        gameState.takeCrystalsFromSack();
        onStartTurn();
    }
    private void endGame() {
        StringBuilder sb = new StringBuilder();
        sb.append("Game has finished! ");
        List<Player> winners = new ArrayList();
        int maxPoint = 0;
        for (Player player : gameState.getPlayers()) {
            if (player.getPoints() == maxPoint) winners.add(player);
            if (player.getPoints() > maxPoint) {
                winners.clear();
                winners.add(player);
                maxPoint = player.getPoints();
            }
            sb.append(player.getName()).append(": ").append(player.getPoints()).append(" ");
        }
        if (winners.size() == 1) {
            Player winner = winners.get(0);
            sb.append(winner.getName()).append(" WINS!");
            LOG.debug(sb.toString());
            movesLogs.addAll(winner.getMovesLog());
            int sum = results.containsKey(winner) ? results.get(winner) + 1 : 1;
            results.put(winner, sum);
            LOG.debug("We have " + movesLogs.size() + " moves!");
            try {
                if (SAVE_MOVES) Utils.saveMovesInFolder(DATASET_FOLDER, movesLogs, 0);
            } catch (Exception ex) {
                LOG.error("Error saving moves log file...", ex);
            }
        } else {
            LOG.debug("Tie!");
        }
    }
    //</editor-fold>

    //<editor-fold desc="Turn management">
    private void onStartTurn() {
        Player currentPlayer = gameState.getCurrentPlayer();
        currentPlayer.onBegingTurn(this);
    }
    public void onCurrentPlayerEndTurn() {
        if (gameState.noMoreCrystalsInFactories()) {
            for (Player player : gameState.getPlayers()) {
                player.pointCount();
                if (player.hasFinished()) {
                    endGame();
                    return;
                }
            }
            gameState.nextPlayer(true);
            gameState.takeCrystalsFromSack();
        } else {
            gameState.nextPlayer(false);
        }
        onStartTurn();
    }
    //</editor-fold>

    public BoardScreen getBoardScreen() {
        return boardScreen;
    }
}
