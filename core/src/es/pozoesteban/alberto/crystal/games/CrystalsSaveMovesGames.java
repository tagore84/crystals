package es.pozoesteban.alberto.crystal.games;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.JuanitoElAleatorio;
import es.pozoesteban.alberto.crystal.model.players.Player;
import es.pozoesteban.alberto.crystal.model.players.ReinforcementLearningStaticPunset;
import es.pozoesteban.alberto.crystal.screens.BoardScreen;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.text.NumberFormat;
import java.util.*;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.Configuration.SaveMovesGameConfiguration.*;


public class CrystalsSaveMovesGames extends CrystalsGame {

    private long start;
    protected final int numPlayers = 2;
    private static Logger LOG = new Logger("CrystalsSaveMovesGames", LOG_LEVEL);

    @Override
    public void create() {
        movesLogs = new ArrayList();
        results = new HashMap();
        numFilesInFolder = Utils.countFilesInFolder(DATASET_FOLDER);
        try {
            for (int i = 0; i < NUM_SETS; i++){
                automaticRandomGamesTrainingModels(-1, numFilesInFolder + i, TRAINING_PLAYER_ONE_CLASS, TRAINING_PLAYER_TWO_CLASS);
            }
            Gdx.app.exit();
        } catch (Exception ex) {
            LOG.error("Fatal error, closing", ex);
        }
    }



    public void playGame() {
        movesLogs = new ArrayList();
        results = new HashMap();
        numFilesInFolder = Utils.countFilesInFolder(DATASET_FOLDER);
        try {
            for (int i = 0; i < NUM_SETS; i++){
                automaticRandomGamesTrainingModels(-1, numFilesInFolder + i, ReinforcementLearningStaticPunset.class, JuanitoElAleatorio.class);
            }
            Gdx.app.exit();
        } catch (Exception ex) {
            LOG.error("Fatal error, closing", ex);
        }
    }


    private void automaticRandomGamesTrainingModels(int firstPlayer, int setNumber, Class... playersType) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        start = new Date().getTime();
        results.clear();
        for (int i = 0; i < GAMES_PER_SET; i++) {
            automaticRandomGame(numPlayers, i, firstPlayer >= 0 ? firstPlayer : i % numPlayers, playersType);
        }
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        defaultFormat.setMinimumFractionDigits(1);
        try {
            if (SAVE_MOVES) Utils.saveMovesInFolder(DATASET_FOLDER, movesLogs, setNumber);
            movesLogs.clear();

            StringBuilder sb = new StringBuilder();
            for (Map.Entry<Player, Integer> entry : results.entrySet()) {
                sb.append(entry.getKey().getName()).append(" has won ").append(defaultFormat.format((float)entry.getValue() / (float)GAMES_PER_SET)).append(System.lineSeparator());
            }
            long milliseconds = new Date().getTime() - start;
            int seconds = (int) (milliseconds / 1000) % 60 ;
            int minutes = (int) ((milliseconds / (1000*60)) % 60);
            int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
            sb.append(System.lineSeparator()).append(GAMES_PER_SET).append(" matchs played in ");
            sb.append(String.format("%02d", hours)).append("h ").append(String.format("%02d", minutes));
            sb.append("' ").append(String.format("%02d", seconds)).append("\"").append(" (set ").append(setNumber+1).append("/").append(NUM_SETS+numFilesInFolder).append(")").append(System.lineSeparator());
            LOG.info(sb.toString());
        } catch (Exception ex) {
            LOG.error("Error saving moves log file...", ex);
        }
    }

    private void automaticRandomGame(int numPlayers, int gameId, int firstPlayer, Class... playersType) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Player> players = new ArrayList();
        for (int i = 0; i < numPlayers; i++) {
            Constructor<?> constructor = playersType[i].getConstructor(Integer.class);
            players.add((Player) constructor.newInstance(new Object[] {i}));
        }
        //FOR DEBUG: players.add(new ReinforcementLearningPunset(1));
        //FOR DEBUG: players.add(new JuanitoElAleatorio(1));

        gameState = new GameState(players, Utils.getNumFactories(numPlayers), firstPlayer);
        startGame(gameId);
    }

    //<editor-fold desc="Match management">
    private void startGame(int gameId) {
        if (gameId % Math.max(1, GAMES_PER_SET/10) == 0) {
            LOG.info("Starting game " + gameId + "/" + GAMES_PER_SET + ", we have " + movesLogs.size() + " moves!");
        }
        gameState.takeCrystalsFromSack();
        onStartTurn();
    }
    private void endGame() {
        StringBuilder sb = new StringBuilder();
        sb.append("Game has finished! ");
        List<Player> winners = new ArrayList();
        int maxPoint = 0;
        for (Player player : gameState.getPlayers()) {
            if (player.getPoints() == maxPoint) winners.add(player);
            if (player.getPoints() > maxPoint) {
                winners.clear();
                winners.add(player);
                maxPoint = player.getPoints();
            }
            sb.append(player.getName()).append(": ").append(player.getPoints()).append(" ");
        }
        if (winners.size() == 1 && winners.get(0).getPoints() >= MIN_POINTS_TO_SAVE_MOVES) {
            Player winner = winners.get(0);
            sb.append(winner.getName()).append(" WINS!");
            LOG.debug(sb.toString());
            movesLogs.addAll(winner.getMovesLog());
            int sum = results.containsKey(winner) ? results.get(winner) + 1 : 1;
            results.put(winner, sum);
            LOG.debug("We have " + movesLogs.size() + " moves!");
        } else {
            LOG.debug("Tie!");
        }
    }
    //</editor-fold>

    //<editor-fold desc="Turn management">
    private void onStartTurn() {
        Player currentPlayer = gameState.getCurrentPlayer();
        currentPlayer.onBegingTurn(this);
    }
    public void onCurrentPlayerEndTurn() {
        if (gameState.noMoreCrystalsInFactories()) {
            for (Player player : gameState.getPlayers()) {
                player.pointCount();
                if (player.hasFinished()) {
                    endGame();
                    return;
                }
            }
            gameState.nextPlayer(true);
            gameState.takeCrystalsFromSack();
        } else {
            gameState.nextPlayer(false);
        }
        onStartTurn();
    }
    //</editor-fold>

    public BoardScreen getBoardScreen() {
        throw new IllegalArgumentException("TrainingGames has no boardScreen!");
    }
}
