package es.pozoesteban.alberto.crystal.games;

public enum GameType {

    REAL(0), SAVE_MOVES(1), DYNAMIC_TRAINING(2);

    private static final long serialVersionUID = 1L;

    private final int value;

    GameType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
