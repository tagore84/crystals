package es.pozoesteban.alberto.crystal.model;

public enum Color {
    YELLOW(0), ORANGE(1), RED(2), WHITE(3), BLUE(4), FIRST_TOKEN(-1);

    private static final long serialVersionUID = 1L;

    private final int value;

    Color(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toChart() {
        switch (this) {
            case YELLOW:
                return "Y";
            case ORANGE:
                return "O";
            case RED:
                return "R";
            case WHITE:
                return "W";
            case BLUE:
                return "B";
            default:
                return "1";
        }
    }
}
