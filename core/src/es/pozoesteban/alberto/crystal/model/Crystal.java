package es.pozoesteban.alberto.crystal.model;

import es.pozoesteban.alberto.crystal.model.players.Player;

public class Crystal implements Comparable {

    private final Color color;
    private final int id;

    private boolean hasChanged;

    private boolean isInTheSack;
    private boolean isInTheGarbage;

    private boolean isInTheFactories;
    private int inFactory;
    private int inFactoryIndex;

    private boolean isInTheBoardPlayers;
    private int inBoardPlayer;
    private int inBoardPlayerColumn;
    private int inBoardPlayerRow;
    private boolean isSelected;

    public Crystal(int id, Color color) {
        this.id = id;
        this.color = color;

        this.hasChanged = false;
        this.isInTheSack = true;
        this.isInTheGarbage = false;
        this.isInTheFactories = false;
        this.inFactory = -1;
        this.inFactoryIndex = -1;
        this.isInTheBoardPlayers = false;
        this.inBoardPlayer = -1;
        this.inBoardPlayerColumn = -1;
        this.inBoardPlayerRow = -1;
        this.isSelected = false;
    }

    public void moveToFinalPanel(int player, int row, int column) {
        hasChanged = true;
        isInTheSack = false;
        isInTheGarbage = false;

        isInTheFactories = false;
        inFactory = -1;
        inFactoryIndex = -1;

        isInTheBoardPlayers = true;
        inBoardPlayer = player;
        inBoardPlayerColumn = column + 5;
        inBoardPlayerRow = row + 1;
    }
    public void moveToSack() {
        hasChanged = true;
        isInTheSack = true;
        isInTheGarbage = false;

        isInTheFactories = false;
        inFactory = -1;
        inFactoryIndex = -1;

        isInTheBoardPlayers = false;
        inBoardPlayer = -1;
        inBoardPlayerColumn = -1;
        inBoardPlayerRow = -1;
    }
    public void moveToGarbage() {
        hasChanged = true;
        isInTheSack = false;
        isInTheGarbage = true;

        isInTheFactories = false;
        inFactory = -1;
        inFactoryIndex = -1;

        isInTheBoardPlayers = false;
        inBoardPlayer = -1;
        inBoardPlayerColumn = -1;
        inBoardPlayerRow = -1;
    }
    public void moveToFactory(int factory, int index) {
        hasChanged = true;
        isInTheSack = false;
        isInTheGarbage = false;

        isInTheFactories = true;
        inFactory = factory;
        inFactoryIndex = index;

        isInTheBoardPlayers = false;
        inBoardPlayer = -1;
        inBoardPlayerColumn = -1;
        inBoardPlayerRow = -1;
    }
    public void moveToPlayerBoard(Player player, int row, int column) {
        hasChanged = true;
        isInTheSack = false;
        isInTheGarbage = false;

        isInTheFactories = false;
        inFactory = -1;
        inFactoryIndex = -1;

        isInTheBoardPlayers = true;
        inBoardPlayer = player.getId();
        inBoardPlayerRow = row;
        inBoardPlayerColumn = column;
    }
    public void moveToCenterPlate(int index) {
        hasChanged = true;
        isInTheSack = false;
        isInTheGarbage = false;

        isInTheFactories = true;
        inFactory = 10;
        inFactoryIndex = index;

        isInTheBoardPlayers = false;
        inBoardPlayer = -1;
        inBoardPlayerRow = -1;
        inBoardPlayerColumn = -1;
    }

    public Color getColor() {
        return color;
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public void hasChanged(boolean b) {
        hasChanged = b;
    }

    public boolean isInTheSack() {
        return isInTheSack;
    }
    public boolean isInTheGarbage() {
        return isInTheGarbage;
    }

    public boolean isInTheFactories() {
        return isInTheFactories;
    }

    public int getInFactoryIndex() {
        return inFactoryIndex;
    }
    public int getInFactory() {
        return inFactory;
    }

    public int getInBoardPlayer() {
        return inBoardPlayer;
    }

    public boolean isInTheBoardPlayers() {
        return isInTheBoardPlayers;
    }

    public int getInBoardPlayerRow() {
        return inBoardPlayerRow;
    }
    public int getInBoardPlayerColumn() {
        return inBoardPlayerColumn;
    }

    public void isSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    public boolean isSelected() {
        return this.isSelected;
    }

    @Override
    public String toString() {
        return color.toString();
    }

    public String toLongString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(").append(id).append(") ").append(color);
        if (isSelected) sb.append(" SELECTED");
        if (isInTheFactories)sb.append(" In the factory ").append(inFactory).append("(").append(inFactoryIndex).append(")");
        if (isInTheGarbage)sb.append(" In the garbage");
        if (isInTheSack) sb.append(" In the sack");
        if (isInTheBoardPlayers) sb.append(" In board of player ").append(inBoardPlayer).append(
                " [").append(inBoardPlayerRow).append(", ").append(inBoardPlayerColumn).append("]");
        return sb.toString();
    }

    public int getId() {
        return id;
    }

    public String whoAndWhere() {
        StringBuilder sb = new StringBuilder();
        sb.append(id).append(" ");
        if (isInTheBoardPlayers) {
            sb.append(" in the board player ").append(inBoardPlayer).append(" row ").append(inBoardPlayerRow).append(" column ").append(inBoardPlayerColumn);
        }
        if (isInTheFactories) {
            sb.append(" in factory ").append(inFactory).append(" index ").append(inFactoryIndex);
        }
        if (isInTheGarbage) {
            sb.append(" in garbage ");
        }
        if (isInTheSack) {
            sb.append(" in the sack ");
        }
        return sb.toString();
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Crystal) {
            Crystal other = (Crystal) o;
            return Integer.compare(id, other.getInFactory());
        }
        throw new IllegalArgumentException("Comparing Crystal with " + o.getClass());
    }
}
