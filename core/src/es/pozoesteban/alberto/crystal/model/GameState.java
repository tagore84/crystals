package es.pozoesteban.alberto.crystal.model;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.players.HumanPlayer;
import es.pozoesteban.alberto.crystal.model.players.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class GameState {

    private final static Logger LOG = new Logger("GameState", LOG_LEVEL);

    private final Crystal[] crystals;
    private final List<Crystal> sack;
    private final List<Player> players;
    private final int numFactories;
    private final List<Crystal> currentSelectedCrystals;
    private final List<Crystal> currentFactoryCrystals;
    private final List<Crystal> centerPlateCrystals;
    private boolean selectingRow;
    private Player currentPlayer;
    private boolean firstCenterPlateTake;
    private final Crystal firstToken;
    private boolean putFirstToken;

    public GameState (List<Player>players, int numFactories, int firstPlayer) {
        firstToken = new Crystal(-1, Color.FIRST_TOKEN);
        firstToken.moveToCenterPlate(0);
        selectingRow = false;
        this.numFactories = numFactories;
        crystals = new Crystal[100];
        sack = new ArrayList();
        currentSelectedCrystals = new ArrayList();
        currentFactoryCrystals = new ArrayList();
        centerPlateCrystals = new ArrayList();
        for (int i = 0; i < crystals.length; i++) {
            crystals[i] = new Crystal(i, Color.values()[i%5]);
            sack.add(crystals[i]);
        }

        this.players = new ArrayList();
        this.players.addAll(players);
        currentPlayer = firstPlayer < 0 ? players.get(Utils.nextInt(players.size())) : players.get(firstPlayer);
    }

    public Crystal[] getCrystals() {
        return this.crystals;
    }

    public void takeCrystalsFromSack() {
        if (sack.size() < (4 * numFactories)) {
            refillSack();
        }
        firstCenterPlateTake = true;

        List<Crystal> toRemove = new ArrayList();
        for (int f = 0; f < numFactories; f++) {
            for (int i = 0; i < 4; i++) {
                Crystal current = sack.get(Utils.nextInt(sack.size()));
                current.moveToFactory(f, i);
                sack.remove(current);
                toRemove.add(current);
            }
        }
        LOG.debug(explain());
    }

    private void refillSack() {
        LOG.debug("Refilling the sack!");
        for (Crystal crystal : crystals) {
            if (crystal.isInTheGarbage()) {
                sack.add(crystal);
                crystal.moveToSack();
            }
        }
    }

    public void takeCrystalsFromFactory(Crystal crystal) {
        currentSelectedCrystals.clear();
        currentFactoryCrystals.clear();
        for (Crystal current : crystals) {
            current.isSelected(false);
        }
        Color color = crystal.getColor();
        int factory = crystal.getInFactory();
        List<Crystal> sameFactory = getCrystalsInFactory(factory);
        putFirstToken = factory == 10 && firstCenterPlateTake;
        for (Crystal current : sameFactory) {
            if (current.getColor() == color) {
                currentSelectedCrystals.add(current);
                current.isSelected(true);
            } else {
                current.isSelected(false);
                currentFactoryCrystals.add(current);
            }
        }
        showRowSelectors();
        /*
        for (Crystal sameFactory : crystals) {
            if (sameFactory.isInTheFactories() && sameFactory.getInFactory() == factory) {
                if (sameFactory.getColor() == color) {
                    currentSelectedCrystals.add(sameFactory);
                    sameFactory.isSelected(true);
                } else {
                    currentFactoryCrystals.add(sameFactory);
                }
            }
        }
        if (currentSelectedCrystals.isEmpty()) throw new IllegalArgumentException("There is no crystals in the factory???");
        showRowSelectors();
         */
    }

    private void showRowSelectors() {
        selectingRow = true;
    }

    public boolean selectingRow(int row) {
        if (selectingRow) {
            Color color = currentSelectedCrystals.get(0).getColor();
            return currentPlayer.isValidRow(color, row);
        } else {
            return false;
        }
    }

    public void selectRow(int row) {
        if (!currentPlayer.isAIPlayer()) {
            HumanPlayer human = (HumanPlayer) currentPlayer;
            human.logMove(this, currentSelectedCrystals.get(0), row);
        }

        if (putFirstToken) {
            currentPlayer.hasFirstPlayerToken(true);
            firstCenterPlateTake = false;
            currentPlayer.putFirstTokenInFloor(firstToken);
        }
        moveCrystalsToRow(row);
        moveCrystalsToCenterPlate();
        currentSelectedCrystals.clear();
        currentFactoryCrystals.clear();
        selectingRow = false;
    }

    private void moveCrystalsToCenterPlate() {
        for (Crystal crystal : currentFactoryCrystals) {
            crystal.moveToCenterPlate(centerPlateCrystals.size());
            centerPlateCrystals.add(crystal);
        }
    }

    private void moveCrystalsToRow(int row) {
        for (Crystal crystal : currentSelectedCrystals) {
            centerPlateCrystals.remove(crystal);
            int[] rowColumn = calcColumnRow(currentPlayer, row);
            if (rowColumn == null) {
                crystal.moveToGarbage();
            } else {
                crystal.moveToPlayerBoard(currentPlayer, rowColumn[0], rowColumn[1]);
                crystal.isSelected(false);
                currentPlayer.putCrystal(crystal);
            }
        }
    }

    private int[] calcColumnRow(Player player, int row) {
        int column = player.getNextEmptySlot(row);
        if (column < 0) return null;
        if (column > 99) {
            row = 0;
            column = column - 100;
        }
        return new int[]{row, column};
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void nextPlayer(boolean newRound) {
        if (newRound) {
            for (Player player : players) {
                if (player.hasFirstPlayerToken()) {
                    currentPlayer = player;
                    return;
                }
            }
        } else {
            switch (players.size()) {
                case 2:
                    currentPlayer = currentPlayer == players.get(0) ? players.get(1) : players.get(0);
                    return;
                case 3:
                    int index = currentPlayer == players.get(0) ? 0 : (currentPlayer == players.get(1) ? 1 : 2);
                    switch (index) {
                        case 0:
                            currentPlayer = players.get(2);
                            return;
                        case 1:
                            currentPlayer = players.get(0);
                            return;
                        case 2:
                            currentPlayer = players.get(1);
                            return;
                    }
                case 4:
                    int index2 = currentPlayer == players.get(0) ? 0 : (currentPlayer == players.get(1) ? 1 : (currentPlayer == players.get(2) ? 2 : 3));
                    switch (index2) {
                        case 0:
                            currentPlayer = players.get(2);
                            return;
                        case 1:
                            currentPlayer = players.get(3);
                            return;
                        case 2:
                            currentPlayer = players.get(1);
                            return;
                        case 3:
                            currentPlayer = players.get(0);
                            return;
                    }
            }
        }
        throw new IllegalArgumentException("We don't go to Ravenholm!");
    }

    public boolean noMoreCrystalsInFactories() {
        for (Crystal crystal : crystals) {
            if (crystal.isInTheFactories()) return false;
        }
        return true;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Crystal getFirstToken() {
        return firstToken;
    }

    public int getNumFactories() {
        return numFactories;
    }

    public List<Crystal> getCrystalsInFactory(int factory) {
        return getCrystalsInFactory(factory, null);
    }
    public List<Crystal> getCrystalsInFactory(int factory, Color color) {
        List<Crystal> output = new ArrayList();
        for (Crystal crystal : crystals) {
            if (crystal.isInTheFactories() && crystal.getInFactory() == factory && (color == null || color.getValue() == crystal.getColor().getValue())) {
                output.add(crystal);
            }
        }
        return output;
    }

    /**
     * Codificación del estado actual. Matriz binaria de 5 por 15, donde cada fila habla de un
     * color y las columnas: las cinco primeras indicas si se puede coger 1, 2, 3, 4 y 5 cristales
     * de ese color exactamente. Las cinco siguientes indican si le caben (exactamente) esos cristales
     * al jugador actual, y las cinco siguientes para el jugador rival.
     * @return estado del juego codificado en una matriz binaria
     */
    public boolean[][] binaryClassCode() {
        boolean[][] code = new boolean[5][15];
        for (int i = 0; i < numFactories; i++) {
            List<Crystal> crystals = getCrystalsInFactory(i);
            int[] crystalsColors = new int[5];
            for (Crystal crystal : crystals) {
                crystalsColors[crystal.getColor().getValue()]++;
            }
            for (int c = 0; c < crystalsColors.length; c++) {
                if (crystalsColors[c] > 0) code[c][crystalsColors[c]-1] = true;
            }
        }
        Player otherPlayer = players.get(0) == currentPlayer ? players.get(1) : players.get(0);
        for (int c = 0; c < 5; c++) {
            for (int s = 1; s < 6; s++) {
                code[c][s+4] = currentPlayer.hasExacSlot(Color.values()[c], s);
                code[c][s+9] = otherPlayer.hasExacSlot(Color.values()[c], s);
            }
        }
        return code;
    }

    public String explain() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numFactories; i++) {
            sb.append("F_").append(i).append(": [ ");
            for (Crystal crystal : getCrystalsInFactory(i)) {
                sb.append(crystal.getColor().toChart()).append(" ");
            }
            sb.append("]" );
        }
        sb.append("F").append(10).append(": [ ");
        for (Crystal crystal : getCrystalsInFactory(10)) {
            sb.append(crystal.getColor().toChart()).append(" ");
        }
        sb.append("] ");
        return sb.toString();
    }

    public boolean canIGetNCrystalColor(int amount, Color color) {
        for (int f = 0; f < numFactories; f++) {
            if (getCrystalsInFactory(f, color).size() == amount) return true;
        }
        return getCrystalsInFactory(10, color).size() == amount;
    }
    public Crystal getCrystalByColorAndAmount(Color color, int amount) {
        for (int f = 0; f < numFactories; f++) {
            if (getCrystalsInFactory(f, color).size() == amount) {
                return getCrystalsInFactory(f, color).get(0);
            }
        }
        return null;
    }
}
