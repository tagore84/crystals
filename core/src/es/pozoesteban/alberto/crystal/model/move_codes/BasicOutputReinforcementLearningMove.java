package es.pozoesteban.alberto.crystal.model.move_codes;

import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.Player;

public class BasicOutputReinforcementLearningMove implements Move {

    /**
     * Codificación del estado actual. Matriz binaria de 5 x 15 (aplanada a 75), donde cada fila habla de un
     * color y las columnas: las cinco primeras indicas si se puede coger 1, 2, 3, 4 y 5 cristales
     * de ese color exactamente. Las cinco siguientes indican si le caben (exactamente) esos cristales
     * al jugador actual, y las cinco siguientes para el jugador rival.
     * @return estado del juego codificado en una matriz binaria
     */
    private float[] crystalSelectionInput;
    /**
     * Matriz de 5 x 5 (aplanada a 25) donde la primera dimensión marca el color de los cristales y la segunda la cantidad de cristales seleccionados.
     * Se rellana con:
     *  · 0 -> En caso de ser un movimiento ilegal
     *  · 1 / totalMovimientosLegales -> en caso de ser un movimiento no seleccionado
     *  · [1 / totalMovimientosLegales o 1] para el movimiento seleccionado. Dependiendo de si se ha perdido o ganado la partida.
     */
    private float[]crystalSelectionOutput;
    /**
     * Array de 6 valores que indical:
     * · El primero: cuántos cristales hay seleccionados.
     * · El resto, cuántos espacios libres disponibles hay para cada fila para el color de cristales seleccionados.
     */
    private float[] rowSelectionInput;
    /**
     * Array de 6 valores que indica:
     * · Para las columnas no seleccionadas: 1/6
     * · Para la columna seleccionada: [0 o 1] Dependiendo de si se ha perdido o ganado la partida.
     */
    private float[] rowSelectionOutput;

    private Player player;
    private GameState gameState;
    private Crystal crystalSelected;
    private int numCrystalsSelected;
    private int rowSelected;
    private Boolean vicotry;

    /*
        Hay que crearlo e irlo rellenando por fases. Que corresponden a su uso.
        1. Creación con el Player y el GameState -> Rellenamos el crystalSelectionInput
        2. Se añade el cristal seleccionado -> Rellenamos rowSelectionInput
        3. Se añade la fila seleccionada -> se guarda...
        4. Se añade si ha ganado o no -> Rellenamos crystalSelectionOutput y rowSelectionOutput
     */

    // 1. Creación con el Player y el GameState -> Rellenamos el crystalSelectionInput
    public BasicOutputReinforcementLearningMove(Player player, GameState gameState) {
        this.player = player;
        this.gameState = gameState;
        boolean[][] binaryGameStateCode = gameState.binaryClassCode();
        int rows = binaryGameStateCode.length;
        int columns = binaryGameStateCode[0].length;
        crystalSelectionInput = new float[rows * columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                crystalSelectionInput[i * columns + j] = binaryGameStateCode[i][j] ? 1.0f : 0.0f;
            }
        }
    }
    // 2. Se añade el cristal seleccionado -> Rellenamos rowSelectionInput
    public void setCrystalSelected(Crystal crystal) {
        crystalSelected = crystal;

        numCrystalsSelected = 0;
        for (Crystal currentCrystal : gameState.getCrystalsInFactory(crystal.getInFactory())) {
            if (currentCrystal.getColor().getValue() == crystal.getColor().getValue()) {
                numCrystalsSelected++;
            }
        }
        /*
        Da igual el color escogido (ya es fijo) para ese color:
        el primer dígito indica cuántos cristales se han cogido y los
        otros cinco indica cuántos caben de ese color en cada fila
         */
        rowSelectionInput = new float[6];
        rowSelectionInput[0] = numCrystalsSelected;
        for (int r = 1; r <= 5; r++) {
            if (player.isValidRow(crystal.getColor(), r)) {
                int emptySlots = 0;
                for (; emptySlots < player.getSlots()[r].length && player.getSlots()[r][emptySlots] == null; emptySlots++);
                rowSelectionInput[r] = emptySlots;
            } else {
                rowSelectionInput[r] = 0;
            }
        }
    }
    // 3. Se añade la fila seleccionada -> se guarda...
    public void setRowSelected(int row) {
        rowSelected = row;
    }
    // 4. Se añade si ha ganado o no -> Rellenamos crystalSelectionOutput y rowSelectionOutput
    public void setVictory(boolean win) {
        vicotry = win;

        crystalSelectionOutput = new float[5 * 5];
        float totalLegalMoves = 0;
        for (int c = 0; c < 5; c++) {
            for (int n = 0; n < 5; n++) {
                if (gameState.canIGetNCrystalColor(n+1, Color.values()[c])) {
                    // ToDo Quizás no haya que hacer esto, y se deba hacer equiprobable sea un movimiento legal o no...
                    totalLegalMoves++;
                }
            }
        }
        for (int c = 0; c < 5; c++) {
            for (int n = 0; n < 5; n++) {
                if (gameState.canIGetNCrystalColor(n+1, Color.values()[c])) {
                    crystalSelectionOutput[c * 5 + n] = 1f / totalLegalMoves;
                } else {
                    crystalSelectionOutput[c * 5 + n] = 0;
                }
            }
        }
        crystalSelectionOutput[crystalSelected.getColor().getValue() * 5 + Math.min(4, numCrystalsSelected-1)] = vicotry ? 1 : 1f / totalLegalMoves;

        rowSelectionOutput = new float[6];
        for (int r = 0; r < rowSelectionOutput.length; r++) {
            if (r == rowSelected) rowSelectionOutput[r] = vicotry ? 1 : 0;
            else rowSelectionOutput[r] = 1f/6f;
        }
    }

    @Override
    public String toSelectCrystalsExample() {
        if (vicotry == null) throw new IllegalArgumentException("BasicOutputReinforcementLearningMove do not completed. Did you win?");
        StringBuilder sb = new StringBuilder();
        sb.append("[[");
        sb.append(floatArrayToString(crystalSelectionInput));
        sb.append("],[");
        sb.append(floatArrayToString(crystalSelectionOutput));
        sb.append("]]");
        return sb.toString();
    }

    @Override
    public String toSelectRowExample() {
        if (vicotry == null) throw new IllegalArgumentException("BasicOutputReinforcementLearningMove do not completed. Did you win?");
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(floatArrayToString(rowSelectionInput));
        sb.append(",");
        sb.append(floatArrayToString(rowSelectionOutput));
        sb.append("]");
        return sb.toString();
    }

    private String booleanArrayToString(boolean[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int c = 0; c < array.length; c++) {
            sb.append(array[c] ? "1.0" : "0.0").append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append("]");
        return sb.toString();
    }

    private String floatArrayToString(float[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int c = 0; c < array.length; c++) {
            sb.append(array[c]).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append("]");
        return sb.toString();
    }

    private String booleanMatirxToString(boolean[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int r = 0; r < matrix.length; r++) {
            sb.append("[");
            for (int c = 0; c < matrix[r].length; c++) {
                sb.append(matrix[r][c] ? "1.0" : "0.0").append(",");
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb.append("],");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    private String floatMatirxToStirng(float[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int r = 0; r < matrix.length; r++) {
            sb.append("[");
            for (int c = 0; c < matrix[r].length; c++) {
                sb.append(matrix[r][c]).append(",");
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb.append("],");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    @Override
    public String toUniqueModelExample() {
        throw new IllegalArgumentException("Move pensado para dos modelos, no para uno único!");
    }

    @Override
    public int getIntWeightByNumCrystals() {
        return 1;
    }

    @Override
    public float getDecimalWeightByNumCrystals() {
        return 0f;
    }

    @Override
    public int getWeightByMaxReward() {
        return 1;
    }

    public float[] getCrystalSelectionInput() {
        return crystalSelectionInput;
    }

    public float[] getCrystalSelectionOutput() {
        return crystalSelectionOutput;
    }

    public float[] getRowSelectionInput() {
        return rowSelectionInput;
    }

    public float[] getRowSelectionOutput() {
        return rowSelectionOutput;
    }

    public Boolean isVictory() {
        return vicotry;
    }
}
