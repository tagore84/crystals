package es.pozoesteban.alberto.crystal.model.move_codes;

public interface Move {

    String toSelectCrystalsExample();

    String toSelectRowExample();

    String toUniqueModelExample();

    int getIntWeightByNumCrystals();

    float getDecimalWeightByNumCrystals();

    int getWeightByMaxReward();
}
