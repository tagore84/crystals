package es.pozoesteban.alberto.crystal.model.move_codes;

import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.players.Player;

public class ReinforcementLearningMove implements Move {

    /**
     * Codificación del estado actual. Matriz binaria de 5 x 15, donde cada fila habla de un
     * color y las columnas: las cinco primeras indicas si se puede coger 1, 2, 3, 4 y 5 cristales
     * de ese color exactamente. Las cinco siguientes indican si le caben (exactamente) esos cristales
     * al jugador actual, y las cinco siguientes para el jugador rival.
     * @return estado del juego codificado en una matriz binaria
     */
    private final boolean[][] crystalSelectionInput;
    /**
     * Matriz de 5 x 5 donde la primera dimensión marca el color de los cristales y la segunda la cantidad de cristales seleccionados.
     * Se rellana con:
     *  · 0 -> En caso de ser un movimiento ilegal
     *  · 1 / totalMovimientosLegales -> en caso de ser un movimiento no seleccionado
     *  · [0, 1] decidido por el método: float calcReward(int freeSlots, int numCrystals) en caso de ser el movimiento seleccionado.
     */
    private final float[][] crystalSelectionOutput;
    /**
     * Array de 6 valores que indical:
     * · El primero: cuántos cristales hay seleccionados.
     * · El resto, cuántos espacios libres disponibles hay para cada fila para el color de cristales seleccionados.
     */
    private final float[] rowModelInput;
    /**
     * Array de 6 valores que indica:
     * · Para las columnas no seleccionadas: 1/6
     * · Para la columna seleccionada: [0, 1] decidido por el método: float calcReward(int freeSlots, int numCrystals) en caso de ser el movimiento seleccionado.
     */
    private final float[] rowModelOutput;

    private final float[] weightByNumCrystals;
    private float maxReward;

    public ReinforcementLearningMove(Player player, GameState gameState, Crystal crystal, int row) {
        crystalSelectionInput = gameState.binaryClassCode();
        crystalSelectionOutput = new float[5][5];
        float total = 0;
        for (int c = 0; c < crystalSelectionOutput.length; c++) {
            for (int n = 0; n < crystalSelectionOutput[c].length; n++) {
                if (gameState.canIGetNCrystalColor(n+1, Color.values()[c])) {
                    // ToDo Quizás no haya que hacer esto, y se deba hacer equiprobable sea un movimiento legal o no...
                    total++;
                }
            }
        }
        for (int c = 0; c < crystalSelectionOutput.length; c++) {
            for (int n = 0; n < crystalSelectionOutput[c].length; n++) {
                if (gameState.canIGetNCrystalColor(n+1, Color.values()[c])) {
                    crystalSelectionOutput[c][n] = 1f / total;
                } else {
                    crystalSelectionOutput[c][n] = 0;
                }
            }
        }

        int numCrystalsSelected = 0;
        for (Crystal currentCrystal : gameState.getCrystalsInFactory(crystal.getInFactory())) {
            if (currentCrystal.getColor().getValue() == crystal.getColor().getValue()) {
                numCrystalsSelected++;
            }
        }
        /*
        Da igual el color escogido (ya es fijo) para ese color:
        el primer dígito indica cuántos cristales se han cogido y los
        otros cinco indica cuántos caben de ese color en cada fila
         */
        rowModelInput  = new float[6];
        rowModelInput[0] = numCrystalsSelected;

        maxReward = 0;
        float[] allRewards = new float[6];
        allRewards[0] = calcReward(0, numCrystalsSelected);
        for (int r = 1; r <= 5; r++) {
            if (player.isValidRow(crystal.getColor(), r)) {
                int emptySlots = 0;
                for (; emptySlots < player.getSlots()[r].length && player.getSlots()[r][emptySlots] == null; emptySlots++);
                rowModelInput[r] = emptySlots;
                allRewards[r] = calcReward(emptySlots, numCrystalsSelected);
            } else {
                allRewards[r] = calcReward(0, numCrystalsSelected);
                rowModelInput[r] = 0;
            }
            if (allRewards[r] > maxReward) maxReward = allRewards[r];
        }

        rowModelOutput = new float[]{(1f/6f), (1f/6f), (1f/6f), (1f/6f), (1f/6f), (1f/6f)};
        rowModelOutput[row] = allRewards[row];

        crystalSelectionOutput[crystal.getColor().getValue()][Math.min(4, numCrystalsSelected-1)] = maxReward;
        weightByNumCrystals = calcWeightByNumCrystals(numCrystalsSelected);
    }

    @Override
    public String toSelectCrystalsExample() {
        StringBuilder sb = new StringBuilder();
        sb.append("[[");
        sb.append(booleanMatrixToStirng(crystalSelectionInput));
        sb.append("],[");
        sb.append(floatMatrixToStirng(crystalSelectionOutput));
        sb.append("]]");
        return sb.toString();
    }

    @Override
    public String toSelectRowExample() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(floatArrayToString(rowModelInput));
        sb.append(",");
        sb.append(floatArrayToString(rowModelOutput));
        sb.append("]");
        return sb.toString();
    }

    private String booleanArrayToString(boolean[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int c = 0; c < array.length; c++) {
            sb.append(array[c] ? "1.0" : "0.0").append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append("]");
        return sb.toString();
    }

    private String floatArrayToString(float[] array) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int c = 0; c < array.length; c++) {
            sb.append(array[c]).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        sb.append("]");
        return sb.toString();
    }

    private String booleanMatrixToStirng(boolean[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int r = 0; r < matrix.length; r++) {
            sb.append("[");
            for (int c = 0; c < matrix[r].length; c++) {
                sb.append(matrix[r][c] ? "1.0" : "0.0").append(",");
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb.append("],");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    private String floatMatrixToStirng(float[][] matrix) {
        StringBuilder sb = new StringBuilder();
        for (int r = 0; r < matrix.length; r++) {
            sb.append("[");
            for (int c = 0; c < matrix[r].length; c++) {
                sb.append(matrix[r][c]).append(",");
            }
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb.append("],");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        return sb.toString();
    }

    @Override
    public String toUniqueModelExample() {
        throw new IllegalArgumentException("Move pensado para dos modelos, no para uno único!");
    }

    @Override
    public int getIntWeightByNumCrystals() {
        return (int)weightByNumCrystals[0];
    }

    @Override
    public float getDecimalWeightByNumCrystals() {
        return weightByNumCrystals[1];
    }

    @Override
    public int getWeightByMaxReward() {
        return (int) (maxReward * 10);
    }

    /**
     * Calcula la recompensa para una elección de colocación de cristales dada.
     * @param freeSlots Número de huecos libres
     * @param numCrystals Número de cristales a colocar
     * @return recompensa del 0 al 1
     */
    private float calcReward(int freeSlots, int numCrystals) {
        float reward = 0F;
        if (freeSlots > 0) {
            if (numCrystals == freeSlots) {
                // Perfecto, encaje de bolillos [0.9, 1]
                reward += 0.9f;
                reward += 0.1f * ((float)numCrystals / 5f);
            } else if (numCrystals > freeSlots) {
                // No está mal, completamos pero se nos caen cristales [0.5, 0.9]
                //reward += 0.5f;
                //reward -= 0.5f * (Math.min(5f, (float)(numCrystals-freeSlots)) / 5f);
                //Mejora gracias al excel de análisis
                reward = 0.8066666668f + (numCrystals * -0.0977777778666666f) + (freeSlots * 0.142222222333333f);
            } else {
                // No está mal, rellenamos aunque no por completo [0.56, 0.75]
                reward += 0.5f + 0.25f * ((float)numCrystals / 4f);
            }
        } else {
            // Todos al suelo, esto es una mierda [0, 0.12]
            reward -= 0.35f;
            reward -= 0.15f * (Math.min(5f, (float)numCrystals) / 5f);
        }
        return Math.max((Math.min(reward, 1f)), 0f);
    }

    /*
    #   sum(likeh)  avg(likeh)  rareFactor  normalizeRareFactor
    1	9.07268129	0.453634065	2.20441999	1
    2	6.99325616	0.349662808	2.859898099	1.297347199
    3	2.658182455	0.132909123	7.523938006	3.413114579
    4	0.886609093	0.044330455	22.55785572	10.23301178
    5	0.352234088	0.017611704	56.78042155	25.75753341
    6	0.037037037	0.001851852	540.0000005	244.9623951
     */
    private float[] calcWeightByNumCrystals(int numCrystals) {
        float[] intDecimalWeight = new float[2];
        switch (numCrystals) {
            case 1:
                intDecimalWeight[0] = 1;
                intDecimalWeight[1] = 0f;
                break;
            case 2:
                intDecimalWeight[0] = 1;
                intDecimalWeight[1] = 0.297347199f;
                break;
            case 3:
                intDecimalWeight[0] = 3;
                intDecimalWeight[1] = 0.413114579f;
                break;
            case 4:
                intDecimalWeight[0] = 10;
                intDecimalWeight[1] = 0.23301178f;
                break;
            case 5:
                intDecimalWeight[0] = 25;
                intDecimalWeight[1] = 0.75753341f;
                break;
            default:
                intDecimalWeight[0] = 244;
                intDecimalWeight[1] = 0.9623951f;
                break;
        }
        return intDecimalWeight;
    }
}
