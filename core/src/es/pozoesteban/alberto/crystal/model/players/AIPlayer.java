package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.model.Crystal;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public abstract class AIPlayer extends Player{

    private final static Logger LOG = new Logger("AIPlayer", LOG_LEVEL);

    public AIPlayer(int id, String name){
        super(id, name);
    }

    protected abstract Crystal calcCristalToTake(CrystalsGame game);

    protected abstract int calcRowToPutCrystal(Crystal crystalTaken);

    @Override
    public void onBegingTurn(CrystalsGame game) {
        Crystal crystalTake = calcCristalToTake(game);

        int row = calcRowToPutCrystal(crystalTake);

        LOG.debug(getName() + " -> Factory: " + crystalTake.getInFactory() + " Color: " + crystalTake.getColor() + " Row: " + row);

        logMove(game.getGameState(), crystalTake, row);


        game.getGameState().takeCrystalsFromFactory(crystalTake);
        game.getGameState().selectRow(row);


        game.onCurrentPlayerEndTurn();
    }

    @Override
    public boolean isAIPlayer() {
        return true;
    }

    public abstract void youWin(boolean win);
}
