package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.move_codes.BasicOutputReinforcementLearningMove;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.buffer.DataType;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.Configuration.DynamicTrainingGameConfiguration.*;

public class DynamicPunset extends AIPlayer {

    private static Logger LOG = new Logger(DynamicPunset.class.getSimpleName(), LOG_LEVEL);

    private static final boolean BEST_OPTION = false;
    private static final boolean VERBOSE = false;

    protected final  JuanitoElAleatorio backup;
    protected GameState currentGameState;
    protected MultiLayerNetwork crystalsSelectorModel;
    protected MultiLayerNetwork rowSelectorModel;

    private BasicOutputReinforcementLearningMove currentMove;
    private List<BasicOutputReinforcementLearningMove> moveBatch;

    public DynamicPunset(Integer id) {
        this(id, "DefaulDynamicPunset", new int[]{50, 35}, new int[]{10});
    }

    public DynamicPunset(int id, String name, int[] hiddenCrystalsLayers, int[] hiddenRowsLayers) {
        super(id, name);
        moveBatch = new ArrayList();
        backup = new JuanitoElAleatorio(-1);
        createOrLoadModels(hiddenCrystalsLayers, hiddenRowsLayers);
    }

    public void youWin(boolean win) {
        for (BasicOutputReinforcementLearningMove move : moveBatch) {
            if (move.isVictory() == null) move.setVictory(win);
        }
    }

    public void trainEpoch() {
        LOG.info("Empieza el entrenamiento de cristales...");
        trainCrystalsModel();
        LOG.info("Termina el entranamiento de cristales y empieza el de filas...");
        trainRowsModel();
        LOG.info("Finaliza el entrenamiento...");
        moveBatch.clear();
    }
    public File[] getModelFiles() {
        File modelDir = new File(DATASET_FOLDER + name);
        File [] crystalsFiles = modelDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".h5") && name.contains("crystals");
            }
        });
        File [] rowsFiles = modelDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".h5") && name.contains("rows");
            }
        });
        if (crystalsFiles == null || crystalsFiles.length == 0
                || rowsFiles == null || rowsFiles.length == 0) {
            return null;
        }
        List<File> crytalsFilesList = new ArrayList();
        for (File models : crystalsFiles) {
            crytalsFilesList.add(models);
        }
        Collections.sort(crytalsFilesList);
        List<File> rowsFilesList = new ArrayList();
        for (File models : rowsFiles) {
            rowsFilesList.add(models);
        }
        Collections.sort(rowsFilesList);
        return new File[]{crytalsFilesList.get(crytalsFilesList.size()-1), rowsFilesList.get(rowsFilesList.size()-1)};
    }
    public File[] getModelFiles(int epoch) {
        File fileCrystals = new File(DATASET_FOLDER
                + name + "/crystals_model_epoch_" + String.format("%03d", epoch)  + ".h5");
        File fileRows = new File(DATASET_FOLDER
                + name + "/rows_model_epoch_" + String.format("%03d", epoch) + ".h5");
        fileCrystals.getParentFile().mkdirs();
        return new File[]{fileCrystals, fileRows};
    }
    public void saveModel(int epoch) {
        try {
            File[] modelFiles = getModelFiles(epoch);
            crystalsSelectorModel.save(modelFiles[0]);
            rowSelectorModel.save(modelFiles[1]);
        } catch (IOException ex) {
            LOG.error("Error saving rows model", ex);
        }
    }

    private void trainCrystalsModel() {
        float[] crystalsInputSample = moveBatch.get(0).getCrystalSelectionInput();
        INDArray crystalsInput = Nd4j.create(moveBatch.size(), crystalsInputSample.length);
        float[] crystalsOutputSample = moveBatch.get(0).getCrystalSelectionOutput();
        INDArray crystalsOutput = Nd4j.create(moveBatch.size(), crystalsOutputSample.length);
        for (int i = 0; i < moveBatch.size(); i++) {
            crystalsInput.add(Nd4j.create(moveBatch.get(i).getCrystalSelectionInput()));
            crystalsOutput.add(Nd4j.create(moveBatch.get(i).getCrystalSelectionOutput()));
        }
        LOG.info("Todo preparado para entrenar cristales...");
        crystalsSelectorModel.fit(crystalsInput, crystalsOutput);
    }
    private void trainRowsModel() {
        float[] rowsInputSample = moveBatch.get(0).getRowSelectionInput();
        INDArray rowsInput = Nd4j.create(moveBatch.size(), rowsInputSample.length);
        float[] rowsOutputSample = moveBatch.get(0).getRowSelectionOutput();
        INDArray rowsOutput = Nd4j.create(moveBatch.size(), rowsOutputSample.length);
        for (int i = 0; i < moveBatch.size(); i++) {
            rowsInput.add(Nd4j.create(moveBatch.get(i).getRowSelectionInput()));
            rowsOutput.add(Nd4j.create(moveBatch.get(i).getRowSelectionOutput()));
        }
        rowSelectorModel.fit(rowsInput, rowsOutput);
    }

    @Override
    protected Crystal calcCristalToTake(CrystalsGame game) {
        currentGameState = game.getGameState();
        moveBatch.add(new BasicOutputReinforcementLearningMove(this, game.getGameState()));
        currentMove = moveBatch.get(moveBatch.size()-1);
        float[] floatGameStateCode = currentMove.getCrystalSelectionInput();
        INDArray input = Nd4j.create(new float[][]{floatGameStateCode});
        INDArray output = crystalsSelectorModel.output(input);
        float[] likelihoodArray = new float[5*5];
        float goodLikelihood = 0;
        float badLikelihood = 0;
        for (int c = 0; c < output.size(0); c++) {
            for (int n = 0; n < output.size(1); n++) {
                float currentLikelihood = output.getFloat(c, n);
                if (currentGameState.canIGetNCrystalColor(n, Color.values()[c])) {
                    likelihoodArray[c * 5 + n] = currentLikelihood;
                    goodLikelihood += currentLikelihood;
                } else {
                    likelihoodArray[c * 5 + n] = 0;
                    badLikelihood += currentLikelihood;
                }
            }
        }
        float[][] likelihood = Utils.arrayToMatrix(likelihoodArray, 5, 5);
        LOG.debug("Good: " + goodLikelihood + ", Bad: " + badLikelihood);
        int[] index;
        if (BEST_OPTION) {
            index = Utils.getBestOption(likelihood);
            if (VERBOSE) {
                LOG.info(DECIMAL_FORMAT.format(likelihood[index[0]][index[1]]) + " / " + DECIMAL_FORMAT.format(goodLikelihood) + " taking " + (index[1]+1) + " " + Color.values()[index[0]] + " crystals");
            }
        } else {
            index = Utils.randomInWeightArray(likelihood, goodLikelihood);
        }
        Crystal crystal = currentGameState.getCrystalByColorAndAmount(Color.values()[index[0]], index[1]+1);

        if (crystal == null) {
            if (VERBOSE) LOG.error("Punset ha intentado coger un color y numero no valido, tiramos de backup aleatorio!");
            return backup.calcCristalToTake(game);
        } else {
            return crystal;
        }
    }

    @Override
    protected int calcRowToPutCrystal(Crystal crystalTaken) {
        currentMove.setCrystalSelected(crystalTaken);
        INDArray input = Nd4j.create(new float[][]{currentMove.getRowSelectionInput()});
        INDArray output = rowSelectorModel.output(input);
        float[] rowsLikelihood = new float[6];
        float sum = 0f;
        for (int i = 0; i < output.length(); i++) {
            rowsLikelihood[i] = isValidRow(crystalTaken.getColor(), i) ? output.getFloat(i) : 0f;
            sum += rowsLikelihood[i];
        }
        LOG.debug("Fila elegida por la IA!");
        if (BEST_OPTION) {
            int bestOption = Utils.getBestOption(rowsLikelihood, sum);
            if (VERBOSE) {
                LOG.info(DECIMAL_FORMAT.format(rowsLikelihood[bestOption]) + " / " + DECIMAL_FORMAT.format(sum) + " choosing row " + bestOption);
            }
            currentMove.setRowSelected(bestOption);
            return bestOption;
        } else {
            int randomOption = Utils.randomInWeightArray(rowsLikelihood, sum);
            currentMove.setRowSelected(randomOption);
            return randomOption;
        }
    }
    private void createOrLoadModels(int[] crystalHiddenLayers, int[] rowsHiddenLayers) {
        File[] files = getModelFiles();
        if (files == null) {
            createCrystalModel(crystalHiddenLayers);
            createRowModel(rowsHiddenLayers);
        } else {
            try {
                this.crystalsSelectorModel = KerasModelImport.importKerasSequentialModelAndWeights(files[0].getAbsolutePath());
                this.rowSelectorModel = KerasModelImport.importKerasSequentialModelAndWeights(files[1].getAbsolutePath());
            } catch (Exception ex) {
                LOG.error("Error cargando los modelos, se crean de cero", ex);
                createCrystalModel(crystalHiddenLayers);
                createRowModel(rowsHiddenLayers);
            }
        }
    }

    private void createCrystalModel(int[] hiddenLayers) {
        NeuralNetConfiguration.ListBuilder listBuilder = new NeuralNetConfiguration.Builder()
                .seed(MODEL_BASE_SEED + id)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new Adam(0.01))
                .list();
        listBuilder.layer(0, new DenseLayer.Builder()
                .nIn(75)
                .nOut(75)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.RELU)
                .build());
        int lastSize = 75;
        for (int h = 0; h < hiddenLayers.length; h++) {
            listBuilder.layer(h+1, new DenseLayer.Builder()
                    .nIn(lastSize)
                    .nOut(hiddenLayers[h])
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build());
            lastSize = hiddenLayers[h];
        }
        listBuilder.layer(hiddenLayers.length + 1, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.SOFTMAX)
                .nIn(lastSize).nOut(25).build());

        listBuilder.setBackpropType(BackpropType.Standard);
        listBuilder.setDataType(DataType.FLOAT);

        crystalsSelectorModel = new MultiLayerNetwork(listBuilder.build());
        INDArray crystalsRandomInput = Nd4j.create(10, 75);
        INDArray crystalsRandomOutput = Nd4j.create(10, 25);

        crystalsSelectorModel.fit(crystalsRandomInput, crystalsRandomOutput);
    }

    private void createRowModel(int[] hiddenLayers) {
        NeuralNetConfiguration.ListBuilder listBuilder = new NeuralNetConfiguration.Builder()
                .seed(MODEL_BASE_SEED + id)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(new Adam(0.01))
                .list();
        int lastSize = 6;
        listBuilder.layer(0, new DenseLayer.Builder()
                .nIn(6)
                .nOut(6)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.RELU)
                .build());
        for (int h = 0; h < hiddenLayers.length; h++) {
            listBuilder.layer(h+1, new DenseLayer.Builder().nIn(lastSize).nOut(hiddenLayers[h])
                    .weightInit(WeightInit.XAVIER)
                    .activation(Activation.RELU)
                    .build());
            lastSize = hiddenLayers[h];
        }
        listBuilder.layer(hiddenLayers.length + 1, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                .weightInit(WeightInit.XAVIER)
                .activation(Activation.SOFTMAX).weightInit(WeightInit.XAVIER)
                .nIn(lastSize).nOut(6).build());

        listBuilder.setBackpropType(BackpropType.Standard);
        listBuilder.setDataType(DataType.FLOAT);

        rowSelectorModel = new MultiLayerNetwork(listBuilder.build());
        INDArray rowsRandomInput = Nd4j.create(10, 6);
        INDArray rowsRandomOutput = Nd4j.create(10, 6);

        rowSelectorModel.fit(rowsRandomInput, rowsRandomOutput);
    }
}
