package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class HumanPlayer extends Player {

    private final static Logger LOG = new Logger("HumanPlayer", LOG_LEVEL);

    public HumanPlayer(Integer id) {
        super(id, "Jugador " + id);
    }

    public HumanPlayer(int id, String name) {
        super(id, name);
    }


    @Override
    public void onBegingTurn(CrystalsGame gameState) {
        LOG.info("Human_onBegingTurn... do nothing...");
        // ToDo Do nothing, I think...
    }

    @Override
    public boolean isAIPlayer() {
        return false;
    }

}
