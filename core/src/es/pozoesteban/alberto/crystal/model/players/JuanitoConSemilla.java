package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;

import java.util.*;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class JuanitoConSemilla extends AIPlayer {

    private final Random dice;

    private static Logger LOG = new Logger("JuanitoConSemilla", LOG_LEVEL);

    public JuanitoConSemilla(Integer id) {
        super(id, "Juanito con Semilla " + id);
        dice = new Random(id);
    }

    @Override
    protected Crystal calcCristalToTake(CrystalsGame game) {
        GameState gameState = game.getGameState();
        List<Crystal> crystalsInFactories = new ArrayList();
        SortedMap<Crystal, List<Integer>> moves = new TreeMap();
        for (Crystal crystal : gameState.getCrystals()) {
            List<Integer> validRows = new ArrayList();
            if (crystal.isInTheFactories()) {
                crystalsInFactories.add(crystal);
                for (int row = 1; row <= 5; row++) {
                    if (isValidRow(crystal.getColor(), row)) {
                        validRows.add(row);
                    }
                }
                if (!validRows.isEmpty()) moves.put(crystal, validRows);
            }
        }
        LOG.debug("Puedo elegir entre " + moves.size() + " cristales!");
        if (moves.isEmpty()) {
            return crystalsInFactories.get(nextInt(crystalsInFactories.size()));
        } else {
            return (Crystal) moves.keySet().toArray()[nextInt(moves.size())];
        }
    }
    @Override
    protected int calcRowToPutCrystal(Crystal crystalTaken) {
        List<Integer> validRows = new ArrayList();
        for (int row= 1; row < 6; row++) {
            if (isValidRow(crystalTaken.getColor(), row)) {
                validRows.add(row);
            }
        }
        if (validRows.isEmpty()) return 0;
        else return validRows.get(nextInt(validRows.size()));
    }

    @Override
    public void youWin(boolean win) {
        // Do nothing here...
    }

    private int nextInt(int max) {
        int output = dice.nextInt(max);
        LOG.debug("Aleatorio hasta " + max + " -> " + output);
        return output;
    }
}
