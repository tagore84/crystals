package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class JuanitoElAleatorio extends AIPlayer {

    private static Logger LOG = new Logger("JuanitoElAleatorio", LOG_LEVEL);


    public JuanitoElAleatorio(Integer id) {
        super(id, "Juanito el Aleatorio " + id);
    }

    @Override
    protected Crystal calcCristalToTake(CrystalsGame game) {
        GameState gameState = game.getGameState();
        List<Crystal> crystalsInFactories = new ArrayList();
        HashMap<Crystal, List<Integer>> moves = new HashMap();
        for (Crystal crystal : gameState.getCrystals()) {
            List<Integer> validRows = new ArrayList();
            if (crystal.isInTheFactories()) {
                crystalsInFactories.add(crystal);
                for (int row= 1; row < 6; row++) {
                    if (isValidRow(crystal.getColor(), row)) {
                        validRows.add(row);
                    }
                }
                if (!validRows.isEmpty()) moves.put(crystal, validRows);
            }
        }
        LOG.debug("Puedo elegir entre " + moves.size() + " cristales!");
        if (moves.isEmpty()) {
            return crystalsInFactories.get(Utils.nextInt(crystalsInFactories.size()));
        } else {
            return (Crystal) moves.keySet().toArray()[Utils.nextInt(moves.size())];
        }
    }
    @Override
    protected int calcRowToPutCrystal(Crystal crystalTaken) {
        List<Integer> validRows = new ArrayList();
        for (int row= 1; row < 6; row++) {
            if (isValidRow(crystalTaken.getColor(), row)) {
                validRows.add(row);
            }
        }
        if (validRows.isEmpty()) return 0;
        else return validRows.get(Utils.nextInt(validRows.size()));
    }

    @Override
    public void youWin(boolean win) {
        // Do nothing here...
    }
}
