package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;
import es.pozoesteban.alberto.crystal.model.GameState;
import es.pozoesteban.alberto.crystal.model.move_codes.ReinforcementLearningMove;
import es.pozoesteban.alberto.crystal.model.move_codes.Move;

import java.text.DecimalFormat;
import java.util.*;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public abstract class Player {

    private final static Logger LOG = new Logger("Player", LOG_LEVEL);

    protected static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    protected Crystal[][] slots;
    protected Crystal[][] finalPanel;
    protected final int id;
    protected final String name;
    protected boolean hasFirstPlayerToken;
    protected int points;

    protected int vicotries;
    protected int defeats;
    protected int ties;
    protected int maxPoints;
    protected int totalPoints;

    protected List<Move> movesLog;

    public Player(int id, String name) {
        this.name = name;
        this.id = id;
        hasFirstPlayerToken = false;
        points = 0;
        finalPanel = new Crystal[5][5];
        slots = new Crystal[6][];
        for (int i = 0; i < 5; i++) {
            slots[i+1] = new Crystal[5-i];
        }
        slots[0] = new Crystal[7];
        movesLog = new ArrayList();

        int vicotries = 0;
        int defeats = 0;
        int ties = 0;
        int maxPoints = 0;
        int totalPoints = 0;
    }

    public int getNextEmptySlot(int row) {
        //Directo al suelo...
        if (row == 0) {
            for (int i = 0; i < slots[0].length; i++) {
                if (slots[0][i] == null) return i + 100;
            }
            return -1; // Ya no hay sitio en el suelo, se descarta
        }
        // Normal
        Crystal[] slotsRow = slots[row];
        for (int i = 0; i < slotsRow.length; i++) {
            if (slotsRow[i] == null) {
                int column =  i + (5 - slotsRow.length);
                return column;
            }
        }
        for (int i = 0; i < slots[0].length; i++) {
            if (slots[0][i] == null) return i + 100;
        }
        return -1; // Ya no hay sitio en el suelo, se descarta
    }

    public void putCrystal(Crystal crystal) {
        int row = crystal.getInBoardPlayerRow();
        int column = crystal.getInBoardPlayerColumn();
        if (row == 0) {
            slots[0][column] = crystal;
        } else {
            slots[row][column - (5 - slots[row].length)] = crystal;
        }
    }

    public boolean isValidRow(Color color, int row) {
        if (row == 0) return true;
        if (finalPanel[row-1][(color.getValue() + (6 - row)) % 5] != null) return false; // Ya tienes ese color en esa fila
        Crystal[] slotsRow = slots[row];
        for (Crystal currentCrystal : slotsRow) {
            if (currentCrystal != null && currentCrystal.getColor() != color) return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public abstract void onBegingTurn(CrystalsGame gameState);
    public abstract boolean isAIPlayer();

    public boolean hasFinished() {
        for (int r = 0; r < finalPanel.length; r++) {
            boolean completeRow = true;
            for (int c = 0; c < finalPanel[r].length; c++) {
                if (finalPanel[r][c] == null) {
                    completeRow = false;
                    break;
                }
            }
            if (completeRow) return true;
        }
        return false;
    }

    public boolean hasFirstPlayerToken() {
        if (hasFirstPlayerToken) {
            hasFirstPlayerToken = false;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return name + " (" + points + " pts)";
    }
    public void hasFirstPlayerToken(boolean hasFirstPlayerToken) {
        this.hasFirstPlayerToken = hasFirstPlayerToken;
    }

    public void pointCount() {
        putCrystalsInFinalPanel();
        manageFloorCrystals();
        clearBoard();
    }

    protected void clearBoard() {
        for (int r = 0; r < slots.length; r++) {
            for (int c = 0; c < slots[r].length; c++) {
                if (slots[r][c] != null) {
                    Crystal current = slots[r][c];
                    if (r == 0) {
                        if (current.getColor() == Color.FIRST_TOKEN) {
                            current.moveToCenterPlate(0);
                        } else {
                            current.moveToGarbage();
                        }
                        slots[r][c] = null;
                    } else {
                        if (slots[r][slots[r].length-1] != null) {
                            if (current.getInBoardPlayerColumn() < 5) current.moveToGarbage();
                            slots[r][c] = null;
                        }
                    }
                }
            }
        }
    }

    private void manageFloorCrystals() {
        int total = 0;
        int[] loosePoints = new int[]{-1, -1, -2, -2, -2, -3, -3};
        for (int i = 0; i < slots[0].length; i++) {
            if (slots[0][i] == null) {
                break;
            } else {
                total += loosePoints[i];
            }
        }
        points = Math.max(0, points + total);
    }


    private void putCrystalsInFinalPanel() {
        for (int i = 5; i > 0; i--) {
            if (slots[i][slots[i].length-1] != null) {
                Crystal toMove = slots[i][5-i];
                int row = i-1;
                int column = (toMove.getColor().getValue() + (6-i)) % 5;
                finalPanel[row][column] = toMove;
                points += pointCrystal(row, toMove.getColor());
                for (Crystal crystal : slots[i]) {
                    crystal.moveToGarbage();
                }
                toMove.moveToFinalPanel(id, row, column);
            }
        }
    }

    protected int pointCrystal(int row, Color color) {
        int column = (color.getValue() + (5-row)) % 5;
        int horizontal = 1;
        for (int i = column+1; i < 5; i++) {
            if (finalPanel[row][i] != null) {
                horizontal++;
            } else {
                break;
            }
        }
        for (int i = column-1; i >= 0; i--) {
            if (finalPanel[row][i] != null) {
                horizontal++;
            } else {
                break;
            }
        }
        int vertical = 1;
        for (int i = row+1; i < 5; i++) {
            if (finalPanel[i][column] != null) {
                vertical++;
            } else {
                break;
            }
        }
        for (int i = row-1; i >= 0; i--) {
            if (finalPanel[i][column] != null) {
                vertical++;
            } else {
                break;
            }
        }
        if (horizontal == 1) horizontal = 0;
        if (vertical == 1) vertical = 0;
        return Math.max(1, horizontal+vertical);
    }

    public void putFirstTokenInFloor(Crystal firstToken){
        for (int i = 0; i < slots[0].length; i++) {
            if (slots[0][i] == null) {
                firstToken.moveToPlayerBoard(this, 0, i);
                slots[0][i] = firstToken;
                return;
            }
        }
    }

    public int getPoints() {
        return points;
    }

    public String getName() {
        return name;
    }

    public List<Move> getMovesLog() {
        return movesLog;
    }


    /**
     * Guarda el movimiento llevado a cabo para poder ser usado para entrenar los sistemas de inteligencia artificial.
     * Para evitar que se sobremuestren los movimientos más comunes (ej: coger un cristal en vez de 4) se logea
     * cada movimiento un número de veces indicado por su rareza (más raro, más veces se logea).
     * @param gameState Estado actual del juego, entrada de la primera red de neuronas.
     * @param crystal Cristal seleccionado (indica color y factoría, el número se deduce).
     * @param row Fila en la que se coloca el cristal, es la salida del segundo modelo.
     */
    public void logMove(GameState gameState, Crystal crystal, int row) {
        Move move;
        if (ReinforcementLearningMove.class.equals(MOVE_CLASS)) {
            move = new ReinforcementLearningMove(this, gameState, crystal, row);
        } else {
            throw new IllegalArgumentException("Invalid Move class " + MOVE_CLASS);
        }

        int intRareFactor = move.getIntWeightByNumCrystals();
        float decimalRareFactor = move.getDecimalWeightByNumCrystals();
        intRareFactor += move.getWeightByMaxReward();

        for (int i = 0; i < intRareFactor; i++) {
            movesLog.add(move);
        }
        if (Utils.nextFloat() < decimalRareFactor) {
            movesLog.add(move);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return id == player.id &&
                name.equals(player.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public boolean hasExacSlot(Color color, int amount) {
        for (int r = 1; r < 6; r++) {
            if (isValidRow(color, r) ) {
                int size = slots[r].length;
                for (int i = 0; i < slots[r].length; i++) {
                    if (slots[r][i] != null) size--;
                }
                if (size == amount) return true;
            }
        }
        return false;
    }

    public Crystal[][] getSlots() {
        return slots;
    }

    public void reset() {
        hasFirstPlayerToken = false;
        points = 0;
        finalPanel = new Crystal[5][5];
        slots = new Crystal[6][];
        for (int i = 0; i < 5; i++) {
            slots[i+1] = new Crystal[5-i];
        }
        slots[0] = new Crystal[7];
    }

    public void saveGamePoints(int ownPoints, int... othersPoints) {
        int result = 1;
        for (int points : othersPoints) {
            if (points > ownPoints) {
                result = -1;
                break;
            } else if (points == ownPoints) {
                result = 0;
            }
        }
        switch (result) {
            case -1:
                defeats++;
                break;
            case 0:
                ties++;
                break;
            default:
                vicotries++;
        }
        if (ownPoints > maxPoints) maxPoints = ownPoints;
        totalPoints += ownPoints;
    }
    public void resetGamePoints() {
        vicotries = 0;
        ties = 0;
        defeats = 0;
        totalPoints = 0;
        maxPoints = 0;
    }
    public String showGamePoints() {
        int plays = vicotries+ties+defeats;
        StringBuilder sb = new StringBuilder();
        sb.append(name)
                .append(" has played ")
                .append(plays)
                .append(" plays and has won ")
                .append(vicotries)
                .append(", max: ")
                .append(maxPoints)
                .append(" pts, avg: ")
                .append((float)totalPoints / (float)plays)
                .append(" pts");
        return sb.toString();
    }
}
