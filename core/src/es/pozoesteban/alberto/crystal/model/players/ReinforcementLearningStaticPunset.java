package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.model.Color;
import es.pozoesteban.alberto.crystal.model.Crystal;
import java.text.DecimalFormat;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class ReinforcementLearningStaticPunset extends StaticPunset {


    private static Logger LOG = new Logger(ReinforcementLearningStaticPunset.class.getSimpleName(), LOG_LEVEL);
    private static final boolean BEST_OPTION = false;
    private static final boolean VERBOSE = false;

    private static final DecimalFormat DF = new DecimalFormat("#.##");

    /**
     * Constructor por defecto para el modo de pruebas/entrenamiento
     * @param id
     */
    public ReinforcementLearningStaticPunset(Integer id) {
        this(id, "ReinforcementLearningPunset");
    }

    public ReinforcementLearningStaticPunset(int id, String name) {
        super(id, name);
    }

    @Override
    protected Crystal calcCristalToTake(CrystalsGame game) {
        currentGameState = game.getGameState();
        boolean[][] binaryGameStateCode = currentGameState.binaryClassCode();
        float[][] floatGameStateCode = new float[binaryGameStateCode[0].length][binaryGameStateCode.length];
        for (int i = 0; i < binaryGameStateCode.length; i++) {
            for (int j = 0; j < binaryGameStateCode[i].length; j++) {
                floatGameStateCode[j][i] = binaryGameStateCode[i][j] ? 1.0f : 0.0f;
            }
        }
        INDArray input = Nd4j.create(new float[][][]{floatGameStateCode});
        INDArray output = crystals_selector_model.output(input);
        float[][] likelihood = new float[5][5];
        float goodLikelihood = 0;
        float badLikelihood = 0;
        for (int c = 0; c < output.size(0); c++) {
            for (int n = 0; n < output.size(1); n++) {
                float currentLikelihood = output.getFloat(c, n);
                if (currentGameState.canIGetNCrystalColor(n, Color.values()[c])) {
                    likelihood[c][n] = currentLikelihood;
                    goodLikelihood += currentLikelihood;
                } else {
                    likelihood[c][n] = 0;
                    badLikelihood += currentLikelihood;
                }
            }
        }
        LOG.debug("Good: " + goodLikelihood + ", Bad: " + badLikelihood);
        int[] index;
        if (BEST_OPTION) {
            index = Utils.getBestOption(likelihood);
            if (VERBOSE) {
                LOG.info(DF.format(likelihood[index[0]][index[1]]) + " / " + DF.format(goodLikelihood) + " taking " + (index[1]+1) + " " + Color.values()[index[0]] + " crystals");
            }
        } else {
            index = Utils.randomInWeightArray(likelihood, goodLikelihood);
        }
        Crystal crystal = currentGameState.getCrystalByColorAndAmount(Color.values()[index[0]], index[1]+1);
        if (crystal == null) {
            if (VERBOSE) LOG.error("Punset ha intentado coger un color y numero no valido, tiramos de backup aleatorio!");
            return backup.calcCristalToTake(game);
        } else {
            return crystal;
        }
    }

    @Override
    protected int calcRowToPutCrystal(Crystal crystal) {
        float[] rowModelInput = new float[6];
        int numCrystalsSelected = currentGameState.getCrystalsInFactory(crystal.getInFactory(), crystal.getColor()).size();
        rowModelInput[0] = numCrystalsSelected;
        for (int r = 1; r <= 5; r++) {
            if (isValidRow(crystal.getColor(), r)) {
                int emptySlots = 0;
                for (; emptySlots < slots[r].length && slots[r][emptySlots] == null; emptySlots++);
                rowModelInput[r] = emptySlots;
            } else {
                rowModelInput[r] = 0;
            }
        }
        INDArray input = Nd4j.create(new float[][]{rowModelInput});
        try {
            INDArray output = row_selector_model.output(input);
            float[] rowsLikelihood = new float[6];
            float sum = 0f;
            for (int i = 0; i < output.length(); i++) {
                rowsLikelihood[i] = isValidRow(crystal.getColor(), i) ? output.getFloat(i) : 0f;
                sum += rowsLikelihood[i];
            }
            LOG.debug("Fila elegida por la IA!");
            if (BEST_OPTION) {
                int bestOption =  Utils.getBestOption(rowsLikelihood, sum);
                if (VERBOSE) {
                    LOG.info(DF.format(rowsLikelihood[bestOption]) + " / " + DF.format(sum) + " choosing row " + bestOption);
                }
                return bestOption;
            } else {
                return Utils.randomInWeightArray(rowsLikelihood, sum);
            }

        } catch (Exception ex) {
            LOG.error("Error predicting row ", ex);
        }
        LOG.error("Punset no ha conseguido seleccionar una fila, eligiendo al azar...");
        return backup.calcRowToPutCrystal(crystal);
    }

    @Override
    public void youWin(boolean win) {
        // Do nothing here...
    }
}
