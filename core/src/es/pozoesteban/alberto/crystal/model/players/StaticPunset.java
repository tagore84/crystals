package es.pozoesteban.alberto.crystal.model.players;

import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.model.GameState;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public abstract class StaticPunset extends AIPlayer {

    private static Logger LOG = new Logger(StaticPunset.class.getSimpleName(), LOG_LEVEL);


    private final static String CRYSTALS_SELECTOR_MODEL_NAME = DATASET_FOLDER + "crystals/";
    private final static String ROWS_SELECTOR_MODEL_NAME = DATASET_FOLDER +  "rows/";


    protected final  JuanitoElAleatorio backup;
    protected GameState currentGameState;
    protected MultiLayerNetwork crystals_selector_model;
    protected MultiLayerNetwork row_selector_model;

    public StaticPunset(int id, String name) {
        super(id, name);
        backup = new JuanitoElAleatorio(-1);
        String[] modelPaths = getModelPaths();
        try {
            LOG.debug("Loading crystals model: " + modelPaths[0]);
            this.crystals_selector_model = KerasModelImport.importKerasSequentialModelAndWeights(modelPaths[0]);
        } catch (Exception ex) {
            LOG.error("Error cargando el modelo " + modelPaths[0], ex);
        }
        try {
            LOG.debug("Loading rows model: " + modelPaths[1]);
            this.row_selector_model = KerasModelImport.importKerasSequentialModelAndWeights(modelPaths[1]);
        } catch (Exception ex) {
            LOG.error("Error cargando el modelo " + modelPaths[1], ex);
        }
    }

    private String[] getModelPaths() {
        String[] output = new String[2];
        File crystalsDir = new File(CRYSTALS_SELECTOR_MODEL_NAME);
        File [] files = crystalsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".h5");
            }
        });
        if (files == null || files.length == 0) {
            throw new IllegalArgumentException("No models files found on " + crystalsDir.getAbsolutePath());
        }
        List<String> names = new ArrayList();
        for (File models : files) {
            names.add(models.getAbsolutePath());
        }
        Collections.sort(names);
        output[0] = names.get(names.size()-1);
        File rowsDir = new File(ROWS_SELECTOR_MODEL_NAME);
        File [] files2 = rowsDir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".h5");
            }
        });
        names = new ArrayList();
        for (File models : files2) {
            names.add(models.getAbsolutePath());
        }
        Collections.sort(names);
        output[1] = names.get(names.size()-1);
        return output;
    }
}
