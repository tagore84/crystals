package es.pozoesteban.alberto.crystal.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import es.pozoesteban.alberto.crystal.CrystalsGame;

public class BaseScreen implements Screen {

    protected CrystalsGame game;
    protected Stage stage;

    public static final float WIDTH_SCREEN = Gdx.graphics.getWidth();
    public static final float HEIGHT_SCREEN = Gdx.graphics.getHeight();
    public static final float MARGIN = 25;

    public BaseScreen(CrystalsGame gameApplication){
        this.game = gameApplication;
    }

    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {

    }

}
