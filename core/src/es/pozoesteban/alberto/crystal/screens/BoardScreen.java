package es.pozoesteban.alberto.crystal.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Logger;
import es.pozoesteban.alberto.crystal.CrystalsGame;
import es.pozoesteban.alberto.crystal.Utils;
import es.pozoesteban.alberto.crystal.actors.*;
import es.pozoesteban.alberto.crystal.model.Crystal;

import java.util.ArrayList;
import java.util.List;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;
import static es.pozoesteban.alberto.crystal.Configuration.RealGameConfiguration.*;

public class BoardScreen extends BaseScreen {

    private final static Logger LOG = new Logger("BoardScreen", LOG_LEVEL);

    public static final float SIZE_FACTOR = 1024f / 3072f; // 0.33f;

    protected PlayerBoardActor[] playerBoardActors;
    protected FactoryActor[] factoryActors;
    protected SackActor sackActor;
    protected PlateActor plateActor;
    protected CrystalActor[] crystalActors;
    protected CrystalActor firstTokenActor;
    protected RowSelectorActor[] rowSelectorActors;
    protected PointsActor pointsActor;



    protected List<ActorDisposable> allBackgroundActors;
    protected List<ActorDisposable> allForegroundActors;

    public BoardScreen(CrystalsGame gameApplication) {
        super(gameApplication);
        init();
    }

    public void init() {
        disposeActors();
        rowSelectorActors = new RowSelectorActor[6];
        playerBoardActors = new PlayerBoardActor[NUM_PLAYERS];
        factoryActors = new FactoryActor[Utils.getNumFactories(NUM_PLAYERS)];
        crystalActors = new CrystalActor[100];
        allBackgroundActors = new ArrayList();
        allForegroundActors = new ArrayList();
    }
    protected void createActors() {
        pointsActor = new PointsActor(game.getGameState());
        allForegroundActors.add(pointsActor);
        for (int i = 0; i < NUM_PLAYERS; i++) {
            playerBoardActors[i] = new PlayerBoardActor(i, game.getGameState());
            allBackgroundActors.add(playerBoardActors[i]);
        }
        for (int i = 0; i < Utils.getNumFactories(NUM_PLAYERS); i++) {
            factoryActors[i] = new FactoryActor(i, NUM_PLAYERS);
            allBackgroundActors.add(factoryActors[i]);
        }
        firstTokenActor = new CrystalActor(game, game.getGameState().getFirstToken());
        allForegroundActors.add(firstTokenActor);
        int index = 0;
        for (Crystal crystal : game.getGameState().getCrystals()) {
            crystalActors[index] = new CrystalActor(game, crystal);
            allForegroundActors.add(crystalActors[index]);
            index++;
        }
        for (int i = 0; i < 6; i++) {
            rowSelectorActors[i] = new RowSelectorActor(game, i);
            allBackgroundActors.add(rowSelectorActors[i]);
        }

        sackActor = new SackActor(game);
        allBackgroundActors.add(sackActor);
        plateActor = new PlateActor();
        allBackgroundActors.add(plateActor);
    }
    protected void addActorsToStage() {
        Group background = new Group();
        Group foreground = new Group();
        stage.addActor(background);
        stage.addActor(foreground);
        for (Actor actor : allBackgroundActors) {
            background.addActor(actor);
        }
        for (Actor actor : allForegroundActors) {
            foreground.addActor(actor);
        }
    }
    protected void disposeActors() {
        if (allBackgroundActors != null) {
            for (ActorDisposable actor : allBackgroundActors) {
                actor.dispose();
            }
        }
        if (allForegroundActors != null) {
            for (ActorDisposable actor : allForegroundActors) {
                actor.dispose();
            }
        }
    }

    @Override
    public void show() {
        super.show();
        stage.setDebugAll(SCREEN_DEBUG);

        createActors();
        addActorsToStage();
    }

    @Override
    public void hide() {
        dispose();
        stage.dispose();
        super.hide();
    }

    @Override
    public void render(float delta) {
        super.render(delta); // ToDo el tutorial no lo hace, stackoverflow sí, mejora el parpadeo? o hay que quitarlo?
        Gdx.gl.glClearColor(0.3f, 0.8f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act();
        stage.draw();
    }

    @Override
    public void dispose() {
        disposeActors();
    }

    public FactoryActor getFactoryActors(int factory) {
        return factoryActors[factory];
    }

    public PlayerBoardActor getPlayerBoardActors(int playerNumber) {
        return playerBoardActors[playerNumber];
    }

    public PlateActor getCenterPlate() {
        return plateActor;
    }

    public float[] calcRandomPositionInTheCenterPlate() {
        float width = crystalActors[0].getWidth();
        float height = crystalActors[0].getHeight();
        float minX = plateActor.getX() + (MARGIN / 2f);
        float maxX = plateActor.getX() + plateActor.getWidth() - width - (MARGIN / 2f);
        float maxY = plateActor.getY() + plateActor.getHeight() - height - (MARGIN / 2f);
        float minY = plateActor.getY() + (MARGIN / 2f);
        int maxIterations = 100;
        float x0 = -1;
        float y0 = -1;
        float x1 = -1;
        float y1 = -1;
        boolean valid = false;

        while (maxIterations-- > 0 && (!valid)) {
            x0 = Utils.nextFloat() * (maxX - minX) + minX;
            y0 = Utils.nextFloat() * (maxY - minY) + minY;
            x1 = x0 + width;
            y1 = y0 + height;
            valid = true;
            List<CrystalActor> all = new ArrayList();
            for (CrystalActor c : crystalActors) {
                all.add(c);
            }
            all.add(firstTokenActor);
            for (CrystalActor crystalActor : all) {
                Crystal crystal = crystalActor.getCrystal();
                if (crystal.isInTheFactories() && crystal.getInFactory() == 10) {
                    float[] pos = new float[]{crystalActor.getX(), crystalActor.getX() + crystalActor.getWidth(),
                    crystalActor.getY(), crystalActor.getY() + crystalActor.getHeight()};
                    if ((x0 >= pos[0] && x0 <= pos[1]) && (y0 >= pos[2] && y0 <= pos[3])) {
                        valid = false;
                        break;
                    }
                    if ((x1 >= pos[0] && x1 <= pos[1]) && (y0 >= pos[2] && y0 <= pos[3])) {
                        valid = false;
                        break;
                    }
                    if ((x1 >= pos[0] && x1 <= pos[1]) && (y1 >= pos[2] && y1 <= pos[3])) {
                        valid = false;
                        break;
                    }
                    if ((x0 >= pos[0] && x0 <= pos[1]) && (y1 >= pos[2] && y1 <= pos[3])) {
                        valid = false;
                        break;
                    }
                }
            }
        }
        if (!valid) LOG.info("No se ha encontrado hueco aleatorio libre!");
        return new float[]{x0, y0};
    }
}
