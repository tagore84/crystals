package es.pozoesteban.alberto.crystal.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import es.pozoesteban.alberto.crystal.games.CrystalsDinamicTraining;
import es.pozoesteban.alberto.crystal.games.CrystalsRealGame;
import es.pozoesteban.alberto.crystal.games.CrystalsSaveMovesGames;
import es.pozoesteban.alberto.crystal.games.GameType;

import static es.pozoesteban.alberto.crystal.Configuration.GeneralConfiguration.*;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Crystals";
		cfg.height = 720;
		cfg.width = 1024;
		cfg.resizable = false;
		cfg.forceExit = false;
		new LwjglApplication(GAME_TYPE == GameType.REAL ? new CrystalsRealGame() :
				(GAME_TYPE == GameType.SAVE_MOVES ? new CrystalsSaveMovesGames() :
						new CrystalsDinamicTraining()), cfg);
	}
}
