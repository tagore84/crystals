import json
import numpy as np
import tensorflow.keras as kr
from statistics import mean, median

version = '_v0.1'
fix = '_fix'
name = 'select_row'

with open(name + '_dataset' + version + fix + '.json', 'r') as file:
    text = file.read().replace('\\r\\n', '')

    data  = json.loads(text)
    prevX = []
    prevY = []
    for move in data:
        prevX.append(move[0])
        prevY.append(move[1])
    X = np.array(prevX)
    Y = np.array(prevY)

print('input: ' + str(X.shape))
print('output: ' + str(Y.shape))

lr = 0.01           # learning rate
nn = [24, 16, 6]  # número de neuronas por capa.

model = kr.Sequential()

# Añadimos la capa 1
l1 = model.add(kr.layers.Dense(nn[0], activation='relu', input_shape=(40,)))

# Añadimos la capa 2
l2 = model.add(kr.layers.Dense(nn[1], activation='relu'))

# Añadimos la capa 3
l3 = model.add(kr.layers.Dense(nn[2], activation='sigmoid'))

# Compilamos el modelo, definiendo la función de coste y el optimizador.\n",
optimizer = kr.optimizers.Adadelta(lr=1.0, rho=0.95, epsilon=None, decay=0.0)
model.compile(loss='mse', optimizer=optimizer, metrics=['acc'])

model.summary()

# Y entrenamos al modelo. Los callbacks
model.fit(X, Y, epochs=100);


output = model.predict(X[0:150])
for i, v in enumerate(output[0]):
    print(str(i) + ': [' + str(min(output[:,i])) + ', ' + ', ' + str(mean(output[:,i])) + ', ' + str(max(output[:,i])) + ']')


model.save(name + '_model' + version + '_TEST.h5')